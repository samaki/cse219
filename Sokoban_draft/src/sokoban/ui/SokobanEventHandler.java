package sokoban.ui;

import java.io.IOException;
import java.util.ArrayList;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import application.Main.SokobanPropertyType;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import properties_manager.PropertiesManager;
import xml_utilities.InvalidXMLFileFormatException;
import sokoban.file.SokobanFileLoader;
import sokoban.game.SokobanGameStateManager;

public class SokobanEventHandler {

    private SokobanUI ui;
    private String levelPath = "file:data/";
    int pos, posCol, posRow;

    /**
     * Constructor that simply saves the ui for later.
     *
     * @param initUI
     */
    public SokobanEventHandler(SokobanUI initUI) {
        ui = initUI;
    }

    /**
     * This method responds to when the user wishes to switch between the Game,
     * Stats, and Help screens.
     *
     * @param uiState The ui state, or screen, that the user wishes to switch
     * to.
     */
    public void respondToSwitchScreenRequest(SokobanUI.SokobanUIState uiState) {

        ui.changeWorkspace(uiState);
    }

    /**
     * This method responds to when the user presses the new game method.
     */
    public void respondToNewGameRequest() {
        SokobanGameStateManager gsm = ui.getGSM();
        gsm.startNewGame();
    }

    /**
     * This method responds to when the user requests to exit the application.
     *
     * @param window The window that the user has requested to close.
     */
    public void respondToExitRequest(Stage primaryStage) {
        // ENGLIS IS THE DEFAULT
        String options[] = new String[]{"Yes", "No"};
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        options[0] = props.getProperty(SokobanPropertyType.DEFAULT_YES_TEXT);
        options[1] = props.getProperty(SokobanPropertyType.DEFAULT_NO_TEXT);
        String verifyExit = props.getProperty(SokobanPropertyType.DEFAULT_EXIT_TEXT);

        // NOW WE'LL CHECK TO SEE IF LANGUAGE SPECIFIC VALUES HAVE BEEN SET
        if (props.getProperty(SokobanPropertyType.YES_TEXT) != null) {
            options[0] = props.getProperty(SokobanPropertyType.YES_TEXT);
            options[1] = props.getProperty(SokobanPropertyType.NO_TEXT);
            verifyExit = props.getProperty(SokobanPropertyType.EXIT_REQUEST_TEXT);
        }

        // FIRST MAKE SURE THE USER REALLY WANTS TO EXIT
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        BorderPane exitPane = new BorderPane();
        HBox optionPane = new HBox();
        optionPane.setAlignment(Pos.CENTER);
        Button yesButton = new Button(options[0]);
        Button noButton = new Button(options[1]);
        optionPane.setSpacing(10.0);
        optionPane.getChildren().addAll(yesButton, noButton);
        Label exitLabel = new Label(verifyExit);
        exitPane.setCenter(exitLabel);
        exitPane.setBottom(optionPane);
        Scene scene = new Scene(exitPane, 200, 100);
        dialogStage.setScene(scene);
        dialogStage.show();
        // WHAT'S THE USER'S DECISION?
        yesButton.setOnAction(e -> {
            // YES, LET'S EXIT
            System.exit(0);
        });
        noButton.setOnAction(e -> {
            dialogStage.close();
        });

    }

    public void respondToSelectLevelRequest(String level, int level2) {

        String filePath = ".\\data\\" + level;
        File fileToOpen = new File(filePath);
        try {
            if (fileToOpen != null) {
                // LET'S USE A FAST LOADING TECHNIQUE. WE'LL LOAD ALL OF THE
                // BYTES AT ONCE INTO A BYTE ARRAY, AND THEN PICK THAT APART.
                // THIS IS FAST BECAUSE IT ONLY HAS TO DO FILE READING ONCE
                byte[] bytes = new byte[Long.valueOf(fileToOpen.length()).intValue()];
                ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                FileInputStream fis = new FileInputStream(fileToOpen);
                BufferedInputStream bis = new BufferedInputStream(fis);

                // HERE IT IS, THE ONLY READY REQUEST WE NEED
                bis.read(bytes);
                bis.close();

                // NOW WE NEED TO LOAD THE DATA FROM THE BYTE ARRAY
                DataInputStream dis = new DataInputStream(bais);

                // NOTE THAT WE NEED TO LOAD THE DATA IN THE SAME
                // ORDER AND FORMAT AS WE SAVED IT
                // FIRST READ THE GRID DIMENSIONS
                int gridColumns = dis.readInt();
                int gridRows = dis.readInt();
                int[][] grid = new int[gridColumns][gridRows];

                // AND NOW ALL THE CELL VALUES
                for (int i = 0; i < gridColumns; i++) {
                    for (int j = 0; j < gridRows; j++) {
                        grid[i][j] = dis.readInt();
                    }
                }
                ui.setGrid(grid);
                ui.setGridCol(gridColumns);
                ui.setGridRow(gridRows);
                ui.getGridR().repaint();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ui.initSokobanUI(level2);
    }

    public void keyPressed(KeyEvent key) {
        KeyCode keC = key.getCode();
        int[] sokobanPos = ui.getSokobanPos();
        int oldX = sokobanPos[0];
        int oldY = sokobanPos[1];

        if (!ui.noKeyControl) {
            if (keC == KeyCode.RIGHT) {
                ui.moveSokoban(oldX, oldY, keC);
            }

            if (keC == KeyCode.LEFT) {
                ui.moveSokoban(oldX, oldY, keC);
            }

            if (keC == KeyCode.UP) {
                ui.moveSokoban(oldX, oldY, keC);
            }

            if (keC == KeyCode.DOWN) {
                ui.moveSokoban(oldX, oldY, keC);
            }

            if (keC == KeyCode.U) {
                undoM();
            }
        }
    }

    public void undoM() {
        boolean eq = Arrays.deepEquals(ui.getGrid(), ui.getUndoStack().peek());
        if (eq) {
            if (ui.getUndoStack().size() == 1) {

            } else {
                ui.getUndoStack().pop();
            }
        }
        ui.undoMove();
    }

    public void mousePressed(MouseEvent m) {
        double w =  ui.getGridR().getWidth() / ui.getGridCol();
        double col =  m.getX() / w;
        double h =  ui.getGridR().getHeight() / ui.getGridRow();
        double row =  m.getY() / h;
        // GET THE VALUE IN THAT CELL
        int value = ui.getGrid()[(int) col][(int) row];
        
        if (value == 4) 
        {
            pos = value;
            posCol = (int) col;
            posRow = (int) row;
        } else 
        {
            if (pos == 4)
            {      
                if(posRow == row)
                {
                    if(posCol == col -1)
                    {
                        ui.moveSokoban(posCol, posRow, KeyCode.LEFT);
                    }
                    if(posCol ==col + 1)
                    {
                        ui.moveSokoban(posCol,posRow,KeyCode.RIGHT);
                    }
                }
                if(posCol == col)
                {
                    if(posRow == row -1)
                    {
                        ui.moveSokoban(posCol,posRow,KeyCode.UP);
                    }
                    if(posRow == row +1)
                    {
                        ui.moveSokoban(posCol,posRow, KeyCode.DOWN);
                    }
                }
            }
        }
    }
}
