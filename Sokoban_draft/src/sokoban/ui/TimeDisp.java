/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sokoban.ui;

import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author momok_000
 */
public class TimeDisp implements Runnable {

    public Label timerLabel;
    long start;
    long current;
    long timeOfGame, hours, minutes, seconds;
    String minutesText, secondsText, time;
    
    public TimeDisp(Label b) {
        timerLabel = b;
        start = System.currentTimeMillis();
    }

    @Override
    public void run() {
        current = System.currentTimeMillis();
        timeOfGame = current - start;
        System.out.println(timeOfGame);
        hours = timeOfGame / (60000);
        timeOfGame -= hours * 60000;
        minutes = timeOfGame / (6000);
        timeOfGame -= minutes * 6000;
        seconds = timeOfGame / 1000;

        // THEN ADD THE TIME OF GAME SUMMARIZED IN PARENTHESES
        minutesText = "" + minutes;
        if (minutes < 10) {
            minutesText = "0" + minutesText;
        }
        secondsText = "" + seconds;
        if (seconds < 10) {
            secondsText = "0" + secondsText;
        }
        time = hours + ":" + minutesText + ":" + secondsText;
        
        Platform.runLater(() -> {
            timerLabel.setText(time);
        });

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {

        }
    }
}
