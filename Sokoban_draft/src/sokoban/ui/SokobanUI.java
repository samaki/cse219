package sokoban.ui;

import application.Main;
import application.Main.SokobanPropertyType;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JEditorPane;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLDocument;

import sokoban.file.SokobanFileLoader;
import sokoban.game.SokobanGameData;
import sokoban.game.SokobanGameStateManager;
import application.Main.SokobanPropertyType;
import java.io.File;
import java.util.Arrays;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.PathTransition;
import javafx.application.Platform;
import properties_manager.PropertiesManager;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class SokobanUI extends Pane {

    /**
     * The SokobanUIState represents the four screen states that are possible
     * for the Sokoban game application. Depending on which state is in current
     * use, different controls will be visible.
     */
    public enum SokobanUIState {

        SPLASH_SCREEN_STATE, PLAY_GAME_STATE, VIEW_STATS_STATE, VIEW_HELP_STATE,
        HANG1_STATE, HANG2_STATE, HANG3_STATE, HANG4_STATE, HANG5_STATE, HANG6_STATE,
    }

    // mainStage
    private Stage primaryStage;

    // mainPane
    private BorderPane mainPane;
    private BorderPane hmPane;

    // SplashScreen
    private ImageView splashScreenImageView;
    private Pane splashScreenPane;
    private Label splashScreenImageLabel;
    private FlowPane levelSelectionPane;
    private ArrayList<Button> levelButtons;

    // NorthToolBar
    private HBox northToolbar;
    private Button gameButton;
    private Button statsButton;
    private Button undoButton;
    private Button exitButton;

    // GamePane
    private Label SokobanLabel;
    private Button newGameButton;
    private HBox letterButtonsPane;
    private HashMap<Character, Button> letterButtons;
    private BorderPane gamePanel = new BorderPane();

    //StatsPane
    private ScrollPane statsScrollPane;
    private JEditorPane statsPane;

    //HelpPane
    private BorderPane helpPanel;
    private JScrollPane helpScrollPane;
    private JEditorPane helpPane;
    private Button homeButton;
    private Pane workspace;

    // Padding
    private Insets marginlessInsets;

    // Image path
    private String ImgPath = "file:images/";

    // mainPane weight && height
    private int paneWidth;
    private int paneHeigth;

    // THIS CLASS WILL HANDLE ALL ACTION EVENTS FOR THIS PROGRAM
    private SokobanEventHandler eventHandler;
    private SokobanErrorHandler errorHandler;
    private SokobanDocumentManager docManager;

    private GraphicsContext graphC;
    private int gridColumns;
    private int gridRows;
    private int grid[][];
    private boolean firstMove;
    private int old;
    private int oldBox;
    private int oldBox2;
    boolean noKeyControl;
    public Label t = new Label();
    Stack<int[][]> undo = new Stack<>();
    private GridRenderer gridR = new GridRenderer();
    SokobanGameStateManager gsm;
    public TimeDisp t2;
    public Thread timeRun;
    private SokobanUIState state;
    public boolean loss;

    public SokobanUI() {
        gsm = new SokobanGameStateManager(this);
        eventHandler = new SokobanEventHandler(this);
        errorHandler = new SokobanErrorHandler(primaryStage);
        docManager = new SokobanDocumentManager(this);
        initMainPane();
        initSplashScreen();
    }

    public void SetStage(Stage stage) {
        primaryStage = stage;
    }

    public BorderPane GetMainPane() {
        return this.mainPane;
    }

    public SokobanGameStateManager getGSM() {
        return gsm;
    }

    public SokobanDocumentManager getDocManager() {
        return docManager;
    }

    public SokobanErrorHandler getErrorHandler() {
        return errorHandler;
    }

    public JEditorPane getHelpPane() {
        return helpPane;
    }

    public GridRenderer getGridR() {
        return gridR;
    }

    public int[][] getGrid() {
        return grid;
    }

    public int getGridCol() {
        return gridColumns;
    }

    public int getGridRow() {
        return gridRows;
    }

    public Stack<int[][]> getUndoStack() {
        return undo;
    }

    public void setGrid(int[][] gr) {
        grid = gr;
    }

    public void setGridCol(int grCol) {
        gridColumns = grCol;
    }

    public void setGridRow(int grRow) {
        gridRows = grRow;
    }

    public void initMainPane() {
        marginlessInsets = new Insets(5, 5, 5, 5);
        mainPane = new BorderPane();

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        paneWidth = Integer.parseInt(props
                .getProperty(SokobanPropertyType.WINDOW_WIDTH));
        paneHeigth = Integer.parseInt(props
                .getProperty(SokobanPropertyType.WINDOW_HEIGHT));
        mainPane.resize(paneWidth, paneHeigth);
        mainPane.setPadding(marginlessInsets);
    }

    public void initSplashScreen() {
        // INIT THE SPLASH SCREEN CONTROLS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String splashScreenImagePath = props
                .getProperty(SokobanPropertyType.SPLASH_SCREEN_IMAGE_NAME);
        props.addProperty(SokobanPropertyType.INSETS, "5");
        String str = props.getProperty(SokobanPropertyType.INSETS);

        splashScreenPane = new StackPane();

        Image splashScreenImage = loadImage(splashScreenImagePath);
        splashScreenImageView = new ImageView(splashScreenImage);
        splashScreenImageView.setFitHeight(725);
        splashScreenImageLabel = new Label();
        splashScreenImageLabel.setGraphic(splashScreenImageView);
        // move the label position to fix the pane
        splashScreenImageLabel.setLayoutX(-45);
        splashScreenPane.getChildren().add(splashScreenImageLabel);

        // GET THE LIST OF LEVEL OPTIONS
        ArrayList<String> levels = props
                .getPropertyOptionsList(SokobanPropertyType.LEVEL_OPTIONS);
        ArrayList<String> levelImages = props
                .getPropertyOptionsList(SokobanPropertyType.LEVEL_IMAGE_NAMES);
        ArrayList<String> levelFiles = props
                .getPropertyOptionsList(SokobanPropertyType.LEVEL_FILES);

        levelSelectionPane = new FlowPane();
        levelSelectionPane.setHgap(10.0);
        levelSelectionPane.setAlignment(Pos.CENTER);
        // add key listener
        levelButtons = new ArrayList<Button>();

        for (int i = 0; i < levels.size(); i++) {

            // GET THE LIST OF LEVEL OPTIONS
            int j = i + 1;
            String level = levels.get(i);
            String levelImageName = levelImages.get(i);
            String levelF = levelFiles.get(i);
            Image levelImage = loadImage(levelImageName);
            ImageView levelImageView = new ImageView(levelImage);
            levelImageView.setFitHeight(110);
            levelImageView.setFitWidth(110);
            // AND BUILD THE BUTTON
            Button levelButton = new Button();
            levelButton.setGraphic(levelImageView);
            levelButton.setText(level);
            // CONNECT THE BUTTON TO THE EVENT HANDLER
            levelButton.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    // TODO
                    eventHandler.respondToSelectLevelRequest(levelF, j);
                    //t2 = new TimeDisp(t);
                    //timeRun = new Thread(t2);
                    //timeRun.start();
                }
            });
            // TODO
            levelSelectionPane.getChildren().add(levelButton);
            // TODO: enable only the first level
            levelButton.setDisable(false);
        }
        splashScreenPane.getChildren().add(levelSelectionPane);
        mainPane.setCenter(splashScreenPane);
        //mainPane.getChildren().add(levelSelectionPane);
        //mainPane.setBottom(levelSelectionPane);
    }

    /**
     * This method initializes the language-specific game controls, which
     * includes the three primary game screens.
     */
    public void initSokobanUI(int level) {

        //it's the first move so set it to true
        firstMove = true;
        // FIRST REMOVE THE SPLASH SCREEN
        mainPane.getChildren().clear();
        noKeyControl = true;
        // GET THE UPDATED TITLE
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(SokobanPropertyType.GAME_TITLE_TEXT);
        primaryStage.setTitle(title);
        gsm.makeNewGame(level);
        // THEN ADD ALL THE STUFF WE MIGHT NOW USE
        initNorthToolbar();

        // OUR WORKSPACE WILL STORE EITHER THE GAME, STATS,
        // OR HELP UI AT ANY ONE TIME
        //initWorkspace();
        initGameScreen();
        //initStatsPane();
        //initHelpPane();
        addUndoHist();
        // WE'LL START OUT WITH THE GAME SCREEN
        changeWorkspace(SokobanUIState.PLAY_GAME_STATE);

    }

    /**
     * This function initializes all the controls that go in the north toolbar.
     */
    private void initNorthToolbar() {
        // MAKE THE NORTH TOOLBAR, WHICH WILL HAVE FOUR BUTTONS
        northToolbar = new HBox();
        northToolbar.setStyle("-fx-background-color:lightgray");
        northToolbar.setAlignment(Pos.CENTER);
        northToolbar.setPadding(marginlessInsets);
        northToolbar.setSpacing(10.0);

        // MAKE AND INIT THE GAME BUTTON
        gameButton = initToolbarButton(northToolbar,
                SokobanPropertyType.GAME_IMG_NAME);
        Image backImage = loadImage("back.png");
        ImageView backImageView = new ImageView(backImage);
        gameButton.setGraphic(backImageView);
        //setTooltip(gameButton, SokobanPropertyType.GAME_TOOLTIP);
        gameButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                // TODO Auto-generated method stub
                if (state != SokobanUIState.VIEW_STATS_STATE) {
                    eventHandler
                            .respondToSwitchScreenRequest(SokobanUIState.SPLASH_SCREEN_STATE);
                    loss = true;
                    gsm.endGame(loss);
                } else {
                    eventHandler
                            .respondToSwitchScreenRequest(SokobanUIState.PLAY_GAME_STATE);
                }

            }
        }
        );

        // MAKE AND INIT THE HELP BUTTON
        undoButton = initToolbarButton(northToolbar,
                SokobanPropertyType.HELP_IMG_NAME);
        Image undoImage = loadImage("undo.png");
        ImageView undoImageView = new ImageView(undoImage);

        undoButton.setGraphic(undoImageView);

        undoButton.setOnAction(
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event
                    ) {
                        // TODO Auto-generated method stub
                        eventHandler
                        .undoM();
                    }

                }
        );

        // MAKE AND INIT THE STATS BUTTON
        statsButton = initToolbarButton(northToolbar,
                SokobanPropertyType.STATS_IMG_NAME);
        Image statsImage = loadImage("stats.png");
        ImageView statsImageView = new ImageView(statsImage);

        statsButton.setGraphic(statsImageView);
        //setTooltip(statsButton, SokobanPropertyType.STATS_TOOLTIP);

        statsButton.setOnAction(
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event
                    ) {
                        // TODO Auto-generated method stub
                        eventHandler
                        .respondToSwitchScreenRequest(SokobanUIState.VIEW_STATS_STATE);
                    }

                }
        );

        // MAKE AND INIT THE EXIT BUTTON
        exitButton = initToolbarButton(northToolbar,
                SokobanPropertyType.EXIT_IMG_NAME);

        exitButton.setText(
                "Exit");
        //setTooltip(exitButton, SokobanPropertyType.EXIT_TOOLTIP);
        exitButton.setOnAction(
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event
                    ) {
                        // TODO Auto-generated method stub
                        eventHandler.respondToExitRequest(primaryStage);
                    }

                }
        );

        Image timeImage = loadImage("timer.png");
        ImageView timeI = new ImageView(timeImage);

        northToolbar.getChildren()
                .add(timeI);
        northToolbar.getChildren()
                .add(t);
        // AND NOW PUT THE NORTH TOOLBAR IN THE FRAME
        mainPane.setTop(northToolbar);
        //mainPane.getChildren().add(northToolbar);
    }

    /**
     * This method helps to initialize buttons for a simple toolbar.
     *
     * @param toolbar The toolbar for which to add the button.
     *
     * @param prop The property for the button we are building. This will
     * dictate which image to use for the button.
     *
     * @return A constructed button initialized and added to the toolbar.
     */
    private Button initToolbarButton(HBox toolbar, SokobanPropertyType prop) {
        // GET THE NAME OF THE IMAGE, WE DO THIS BECAUSE THE
        // IMAGES WILL BE NAMED DIFFERENT THINGS FOR DIFFERENT LANGUAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imageName = props.getProperty(prop);

        // LOAD THE IMAGE
        Image image = loadImage(imageName);
        ImageView imageIcon = new ImageView(image);

        // MAKE THE BUTTON
        Button button = new Button();
        button.setGraphic(imageIcon);
        button.setPadding(marginlessInsets);

        // PUT IT IN THE TOOLBAR
        toolbar.getChildren().add(button);

        // AND SEND BACK THE BUTTON
        return button;
    }

    public void initGameScreen() {
        //gamePanel.setCenter(gridR);
        mainPane.setCenter(gridR);
        mainPane.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent key) {
                // TODO Auto-generated method stub
                eventHandler.keyPressed(key);
            }
        });

    }

    public void initStatsScreen() {

    }

    public void createEndDialog() {
        Stage dialogStage = new Stage();
        Label resultLabel = new Label();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        Button btn = new Button("Ok");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                changeWorkspace(SokobanUIState.SPLASH_SCREEN_STATE);
                dialogStage.close();
            }
        });

        if (loss) {
            Media m2 = new Media(new File(".\\data\\" + "gameover.mp3").toURI().toString());
            MediaPlayer mediaPlayer2 = new MediaPlayer(m2);
            dialogStage.setTitle("Loser!");
            resultLabel.setText("You have failed to beat the level. Try again!\n\n");
            mediaPlayer2.play();
        } else {
            Media m1 = new Media(new File(".\\data\\" + "tada.mp3").toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(m1);
            dialogStage.setTitle("Winner!!!!!!!!");
            resultLabel.setText("You have beaten the level. Congratulations!!\n\n");
            mediaPlayer.play();
        }

        resultLabel.setAlignment(Pos.BASELINE_CENTER);
        VBox vB = new VBox();
        vB.setAlignment(Pos.CENTER);
        vB.getChildren().addAll(resultLabel, btn);
        dialogStage.setScene(new Scene(vB, 250, 100));
        dialogStage.show();
    }

    public void addUndoHist() {
        int[][] u3 = new int[gridColumns][gridRows];
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                u3[i][j] = grid[i][j];
            }
        }
        undo.push(u3);
    }

    /**
     * The workspace is a panel that will show different screens depending on
     * the user's requests.
     */
    private void initWorkspace() {
        // THE WORKSPACE WILL GO IN THE CENTER OF THE WINDOW, UNDER THE NORTH
        // TOOLBAR
        workspace = new Pane();
        mainPane.setCenter(workspace);
        //mainPane.getChildren().add(workspace);
        System.out.println("in the initWorkspace");
    }

    public Image loadImage(String imageName) {
        Image img = new Image(ImgPath + imageName);
        return img;
    }

    /**
     * This function selects the UI screen to display based on the uiScreen
     * argument. Note that we have 3 such screens: game, stats, and help.
     *
     * @param uiScreen The screen to be switched to.
     */
    public void changeWorkspace(SokobanUIState uiScreen) {
        switch (uiScreen) {
            case SPLASH_SCREEN_STATE:
                mainPane.getChildren().clear();
                noKeyControl = true;
                state = SokobanUIState.SPLASH_SCREEN_STATE;
                undo.clear();
                mainPane.setCenter(splashScreenPane);
                break;
            case PLAY_GAME_STATE:
                mainPane.getChildren().clear();
                initNorthToolbar();
                noKeyControl = false;
                state = SokobanUIState.PLAY_GAME_STATE;
                mainPane.setCenter(gridR);
                break;
            case VIEW_STATS_STATE:
                noKeyControl = true;
                mainPane.setCenter(statsScrollPane);
                state = SokobanUIState.VIEW_STATS_STATE;
                break;
            default:
        }
    }

    public void moveSokoban(int oldX, int oldY, KeyCode x) {
        int newX = oldX;
        int newY = oldY;
        Media m1 = new Media(new File(".\\data\\" + "move.wav").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(m1);
        Media m2 = new Media(new File(".\\data\\" + "nomove.wav").toURI().toString());
        MediaPlayer mediaPlayer2 = new MediaPlayer(m2);

        if (x == KeyCode.RIGHT) {
            newX = oldX + 1;
        } else if (x == KeyCode.LEFT) {
            newX = oldX - 1;
        } else if (x == KeyCode.UP) {
            newY = oldY - 1;
        } else if (x == KeyCode.DOWN) {
            newY = oldY + 1;
        }

        switch (grid[newX][newY]) {
            case 1:
                mediaPlayer2.play();
                return;
            case 2:
                boolean bMove = moveBox(newX, newY, x);
                if (bMove) {
                    break;
                } else {
                    mediaPlayer2.play();
                    return;
                }
            case 3:
                break;
        }

        if (firstMove) {
            mediaPlayer.play();
            firstMove = false;
            old = grid[newX][newY];
            grid[newX][newY] = 4;
            grid[oldX][oldY] = 0;
            addUndoHist();
            this.gridR.repaint();
        } else {
            mediaPlayer.play();
            grid[oldX][oldY] = old;
            old = grid[newX][newY];
            grid[newX][newY] = 4;
            this.gridR.repaint();
            addUndoHist();
        }
    }

    public boolean moveBox(int oldX, int oldY, KeyCode key) {
        int newX = oldX;
        int newY = oldY;

        if (key == KeyCode.RIGHT) {
            newX = oldX + 1;
        } else if (key == KeyCode.LEFT) {
            newX = oldX - 1;
        } else if (key == KeyCode.UP) {
            newY = oldY - 1;
        } else if (key == KeyCode.DOWN) {
            newY = oldY + 1;
        }

        switch (grid[newX][newY]) {
            case 1:
                return false;
            case 2:
                return false;
        }

        if (checkBoxInitialMove(oldX, oldY)) {
            oldBox2 = grid[newX][newY];
            System.out.println(oldBox2);
            grid[newX][newY] = 2;
            grid[oldX][oldY] = 0;
            this.gridR.repaint();
        } else {
            grid[oldX][oldY] = oldBox2;
            oldBox2 = grid[newX][newY];
            grid[newX][newY] = 2;
            System.out.println(oldBox2);
            this.gridR.repaint();
        }

        if (gsm.checkWin() || gsm.checkLoss(newX, newY, oldBox2)) {
            createEndDialog();
            gsm.endGame(loss);
        }

        return true;
    }

    public boolean checkBoxInitialMove(int boxX, int boxY) {
        int[][] boxCheck = undo.firstElement();
        if (grid[boxX][boxY] == boxCheck[boxX][boxY]) {
            return true;
        }
        return false;
    }

    public void undoMove() {
        int[][] u2 = new int[gridColumns][gridRows];
        if (undo.size() == 1) {
            u2 = undo.peek();
        } else {
            u2 = undo.pop();
        }
        int[][] u3 = new int[gridColumns][gridRows];
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                u3[i][j] = u2[i][j];
            }
        }

        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                grid[i][j] = u3[i][j];
            }
        }
        System.out.println();
        this.gridR.repaint();
    }

    public int[] getSokobanPos() {

        double w = this.getWidth() / gridColumns;
        double h = this.getHeight() / gridRows;

        int[] s2 = {0, 0};
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {

                if (grid[i][j] == 4) {
                    s2[0] = i;
                    s2[1] = j;
                }
            }
        }
        return s2;
    }

    class GridRenderer extends Canvas {

        // PIXEL DIMENSIONS OF EACH CELL
        int cellWidth;
        int cellHeight;

        // images
        Image wallImage = new Image("file:images/wall.png");
        Image boxImage = new Image("file:images/box.png");
        Image placeImage = new Image("file:images/place.png");
        Image sokobanImage = new Image("file:images/Sokoban.png");

        public GridRenderer() {
            this.setWidth(600);
            this.setHeight(600);
        }

        public void repaint() {
            graphC = this.getGraphicsContext2D();
            graphC.clearRect(0, 0, this.getWidth(), this.getHeight());

            // CALCULATE THE GRID CELL DIMENSIONS
            double w = this.getWidth() / gridColumns;
            double h = this.getHeight() / gridRows;

            graphC = this.getGraphicsContext2D();

            // NOW RENDER EACH CELL
            int x = 0, y = 0;
            for (int i = 0; i < gridColumns; i++) {
                y = 0;
                for (int j = 0; j < gridRows; j++) {

                    switch (grid[i][j]) {
                        case 0:
                            graphC.setFill(Color.WHITE);
                            graphC.fillRect(x, y, w, h);
                            break;
                        case 1:
                            graphC.drawImage(wallImage, x, y, w, h);
                            break;
                        case 2:
                            graphC.drawImage(boxImage, x, y, w, h);
                            break;
                        case 3:
                            graphC.drawImage(placeImage, x, y, w, h);
                            break;
                        case 4:
                            graphC.drawImage(sokobanImage, x, y, w, h);
                            break;
                    }

                    // ON TO THE NEXT ROW
                    y += h;
                }
                // ON TO THE NEXT COLUMN
                x += w;
            }
        }
    }
}
