/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.ui;

import JTE.game.CardData;
import JTE.game.City;
import JTE.game.JourneyGameData;
import JTE.game.JourneyPlayer;
import java.util.ArrayList;
import java.util.Random;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Kwun Chan
 */
public class JourneyEventHandler {

    private JourneyUI ui;
    private JourneyGameData currentGame;
    private ArrayList<JourneyPlayer> players;
    private String[] s;

    public JourneyEventHandler(JourneyUI jUI) {
        ui = jUI;
    }

    public void respondToQuitRequest() {
        System.exit(0);
    }

    public void changeWorkspace(JourneyUI.JourneyUIState state) {
        ui.changeWorkspace(state);
    }

    public void startGame(int selectedNum, JourneyUI.JourneyUIState state, int seed1, int seed2 , int seed3) {
        ui.getGSM().makeNewGame(selectedNum);
        ui.initGamePlayScreen(seed3);
        changeWorkspace(state);
        ui.createCityButtons();
        ui.createFlightButtons();
        ui.getGSM().startNewGame();
        currentGame = ui.getGSM().getGame();
        players = currentGame.getPlayers();
        s = ui.getGSM().getGame().getColors();
        initDeal(seed1);
        restDeal(seed2);
        int curPlayer = currentGame.getPlayerTurn();
        initPlayers(curPlayer);
    }

    public void initDeal(int seed) {
        Random rand = new Random(seed);
        int card = 0;
        for (int i = 0; i < players.size(); i++) {

            if (i >= s.length) {
                card = i - s.length;
            } else {
                card = i;
            }
            JourneyPlayer player = players.get(i);
            String col = s[card];
            whichCard(col, rand, player);
        }
    }

    public void restDeal(int seed) {
        Random rand = new Random(seed);
        int card = 0;
        for (int i = 0; i < players.size(); i++) {

            if (i >= s.length - 1) {
                if (i == 5) {
                    card = 0;
                } else {
                    card = i - (s.length - 1);
                }
            } else {
                card = i + 1;
            }
            JourneyPlayer player = players.get(i);
            String col = s[card];
            whichCard(col, rand, player);
        }

        for (int i = 0; i < players.size(); i++) {

            if (i >= s.length - 2) {
                if (i >= 4) {
                    card = i - (s.length + 1);
                } else {
                    card = i - (s.length - 2);
                }
            } else {
                card = i + 2;
            }
            JourneyPlayer player = players.get(i);
            String col = s[card];
            whichCard(col, rand, player);
        }
    }

    public void whichCard(String colo, Random rand, JourneyPlayer player) {
        switch (colo) {
            case "Red": {
                ArrayList<CardData> curRed = currentGame.getRed();
                int ran = rand.nextInt(curRed.size());
                CardData c = curRed.get(ran);
                player.getCards().add(c);
                curRed.remove(ran);
                break;
            }
            case "Green": {
                ArrayList<CardData> curGreen = currentGame.getGreen();
                int ran = rand.nextInt(curGreen.size());
                CardData c = curGreen.get(ran);
                player.getCards().add(c);
                curGreen.remove(ran);
                break;
            }
            case "Yellow": {
                ArrayList<CardData> curYellow = currentGame.getYellow();
                int ran = rand.nextInt(curYellow.size());
                CardData c = curYellow.get(ran);
                player.getCards().add(c);
                curYellow.remove(ran);
                break;
            }
        }
    }

    public void endTurnPrematurely() {
        int curPlayer = currentGame.getPlayerTurn();
        JourneyPlayer currP = currentGame.getPlayers().get(curPlayer - 1);
        currP.setRollAgain(false);
        endTurn();
    }

    public void endTurn() {
        int curPlayer = currentGame.getPlayerTurn();
        JourneyPlayer currP = currentGame.getPlayers().get(curPlayer - 1);

        if (currP.getRAgain()) {
            currP.setFly(false);
            currP.setJustFlew(false);
            ui.getFlightButton().setDisable(false);
            for (int i = 0; i < currP.getThisTurnMoves().size(); i++) {
                currP.getThisTurnMoves().remove(i);
                i--;
            }
            startPlayerTurn(curPlayer);
        } else {
            if (currentGame.getPlayerTurn() < players.size()) {
                currP.setFly(false);
                currP.setJustFlew(false);
                ui.setRoll(false);
                for (int i = 0; i < currP.getThisTurnMoves().size(); i++) {
                    currP.getThisTurnMoves().remove(i);
                    i--;
                }
                currentGame.setPlayerTurn(currentGame.getPlayerTurn() + 1);
                startPlayerTurn(currentGame.getPlayerTurn());
            } else {
                currP.setFly(false);
                currP.setJustFlew(false);
                ui.setRoll(false);
                for (int i = 0; i < currP.getThisTurnMoves().size(); i++) {
                    currP.getThisTurnMoves().remove(i);
                    i--;
                }
                currentGame.setPlayerTurn(1);
                startPlayerTurn(currentGame.getPlayerTurn());
            }
        }
    }

    public void initPlayers(int player) {
        ui.createCardList((player - 1));
    }

    public void startPlayerTurn(int player) {
        currentGame.setPlayerTurn(player);
        int curPlayer = currentGame.getPlayerTurn();
        ui.setupTurn(player);
    }

    public void playerMoves(int roll) {
        int curPlayer = currentGame.getPlayerTurn();
        JourneyPlayer currP = currentGame.getPlayers().get(curPlayer - 1);
        currP.getRolls().add(roll);
        ui.getDocManager().addRollsToDoc(curPlayer);

        if (roll == 6) {
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(ui.getPrimaryStage());
            VBox noD = new VBox();
            Label s = new Label("You may roll again.");
            Button ok = new Button("Okay");
            ok.setAlignment(Pos.CENTER);
            ok.setStyle(ui.getButStyle());
            ok.setOnAction((ActionEvent et) -> {
                dialogStage.close();
            });
            noD.getChildren().addAll(s, ok);
            noD.setSpacing(5);
            noD.setAlignment(Pos.CENTER);
            Scene scene = new Scene(noD, 150, 100);
            dialogStage.initStyle(StageStyle.UNDECORATED);
            dialogStage.setScene(scene);
            dialogStage.show();
            currP.setRollAgain(true);
        }
        currP.setMovePoints(roll);
    }

    public void movePlayer(String buttonName, ArrayList<ImageView> pieces, ArrayList<Integer> xCoords, ArrayList<Integer> yCoords) {
        String butName = buttonName;
        int curPlayer = currentGame.getPlayerTurn();
        JourneyPlayer currP = currentGame.getPlayers().get(curPlayer - 1);
        ImageView p = pieces.get(curPlayer - 1);
        int pxCoord = xCoords.get(curPlayer - 1);
        int pyCoord = yCoords.get(curPlayer - 1);
        City curC = null;
        ArrayList<String> turnMoves = currP.getThisTurnMoves();
        ArrayList<String> totalMoves = currP.getTotalMoves();
        boolean moveable = true;

        if (butName.equals(currP.getCurrentCity())) {
            ui.createDialog("You are at this city", 200, 100);
        } else {
            for (City c : ui.getMap().getCities()) {
                if (c.getCityName().equals(currP.getCurrentCity())) {
                    curC = c;
                }
            }
            turnMoves.add(curC.getCityName());
            ArrayList<String> land = curC.landNeighbors;
            ArrayList<String> sea = curC.seaNeighbors;
            for (int i = 0; i < land.size(); i++) {
                if (butName.equals(land.get(i))) {
                    for (JourneyPlayer pl : currentGame.getPlayers()) {
                        if (pl.getPlayerNum() != currP.getPlayerNum()) {
                            if (pl.getCurrentCity().equals(butName)) {
                                moveable = false;
                            } else {
                                moveable = true;
                            }
                        }
                    }
                    if (!moveable) {
                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                    } else {
                        if (turnMoves.get(turnMoves.size() - 1).equals(butName) && !currP.getFly()) {
                            ui.createDialog("You can not go back to a previous city in the same turn!!", 600, 200);
                        } else {
                            currP.setMovePoints(currP.getMovePoints() - 1);
                            totalMoves.add(curC.getCityName());
                            ui.getDocManager().addMovesToDoc(curPlayer);
                            ui.moveChar(butName, p, pxCoord, pyCoord);
                            currP.setCurrentCity(butName);

                        }
                    }
                }
            }

            for (int i = 0; i < sea.size(); i++) {
                if (butName.equals(sea.get(i))) {
                    if (currP.getHarb()) {
                        for (JourneyPlayer pl : currentGame.getPlayers()) {
                            if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                if (pl.getCurrentCity().equals(butName)) {
                                    moveable = false;
                                } else {
                                    moveable = true;
                                }
                            }
                        }
                        if (!moveable) {
                            ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                        } else {
                            if (turnMoves.get(turnMoves.size() - 1).equals(butName) && !currP.getFly()) {
                                ui.createDialog("You can not go back to a previous city in the same turn!!", 600, 200);
                            } else {
                                currP.setMovePoints(currP.getMovePoints() - 1);
                                totalMoves.add(curC.getCityName());
                                ui.getDocManager().addMovesToDoc(curPlayer);
                                ui.moveChar(butName, p, pxCoord, pyCoord);
                                currP.setCurrentCity(butName);
                                currP.setHarb(false);
                            }
                        }
                    } else {
                        Stage dialogStage = new Stage();
                        dialogStage.initModality(Modality.WINDOW_MODAL);
                        dialogStage.initOwner(ui.getPrimaryStage());
                        VBox noD = new VBox();
                        Label s = new Label("You will need to stop here for one turn. End This Turn?");
                        Button ok = new Button("Yes");
                        ok.setStyle(ui.getButStyle());
                        ok.setOnAction((ActionEvent et) -> {
                            currP.setHarb(true);
                            endTurn();
                            dialogStage.close();
                        });
                        Button no = new Button("No");
                        no.setStyle(ui.getButStyle());
                        no.setOnAction((ActionEvent et) -> {
                            dialogStage.close();
                        });
                        HBox buts = new HBox();
                        buts.setSpacing(5);
                        buts.setAlignment(Pos.CENTER);
                        buts.getChildren().addAll(ok, no);
                        noD.getChildren().addAll(s, buts);
                        noD.setSpacing(5);
                        noD.setAlignment(Pos.CENTER);
                        Scene scene = new Scene(noD, 400, 70);
                        dialogStage.initStyle(StageStyle.UNDECORATED);
                        dialogStage.setScene(scene);
                        dialogStage.show();
                    }
                }
            }
        }
        currP.setFly(false);
    }

    public void handleFlight(String buttonName, ArrayList<ImageView> pieces, ArrayList<Integer> xCoords, ArrayList<Integer> yCoords) {
        String butName = buttonName;
        int curPlayer = currentGame.getPlayerTurn();
        JourneyPlayer currP = currentGame.getPlayers().get(curPlayer - 1);
        City curC = null;
        City nCity = null;

        for (City c : ui.getMap().getCities()) {
            if (c.getCityName().equals(currP.getCurrentCity())) {
                curC = c;
            }
        }

        for (City c : ui.getMap().getCities()) {
            if (c.getCityName().equals(buttonName)) {
                nCity = c;
            }
        }

        if (currP.getJustFlew()) {
            ui.createDialog("You flew already this turn.!! You have to wait till next turn!", 500, 150);
        } else {
            int curairPortSec = curC.getAirport();
            switch (curairPortSec) {
                case 1:
                    if (nCity.getAirport() != 2 && nCity.getAirport() != 4 && nCity.getAirport() != curairPortSec) {
                        ui.createDialog("You can not fly there!!\n Pick a different city.", 200, 150);
                    } else if (nCity.getAirport() == curairPortSec) {
                        if (currP.getMovePoints() < 2) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (2 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    } else {
                        if (currP.getMovePoints() < 4) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (4 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    }
                    break;
                case 2:
                    if (nCity.getAirport() != 1 && nCity.getAirport() != 3 && nCity.getAirport() != curairPortSec) {
                        ui.createDialog("You can not fly there!!\n Pick a different city.", 200, 150);
                    } else if (nCity.getAirport() == curairPortSec) {
                        if (currP.getMovePoints() < 2) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (2 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    } else {
                        if (currP.getMovePoints() < 4) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (4 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    }
                    break;
                case 3:
                    if (nCity.getAirport() != 2 && nCity.getAirport() != 4 && nCity.getAirport() != 6 && nCity.getAirport() != curairPortSec) {
                        ui.createDialog("You can not fly there!!\n Pick a different city.", 200, 150);
                    } else if (nCity.getAirport() == curairPortSec) {
                        if (currP.getMovePoints() < 2) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (2 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    } else {
                        if (currP.getMovePoints() < 4) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (4 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    }
                    break;
                case 4:
                    if (nCity.getAirport() != 5 && nCity.getAirport() != 1 && nCity.getAirport() != 3 && nCity.getAirport() != curairPortSec) {
                        ui.createDialog("You can not fly there!!\n Pick a different city.", 200, 150);
                    } else if (nCity.getAirport() == curairPortSec) {
                        if (currP.getMovePoints() < 2) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (2 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    } else {
                        if (currP.getMovePoints() < 4) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (4 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    }
                    break;
                case 5:
                    if (nCity.getAirport() != 6 && nCity.getAirport() != 4 && nCity.getAirport() != curairPortSec) {
                        ui.createDialog("You can not fly there!!\n Pick a different city.", 200, 150);
                    } else if (nCity.getAirport() == curairPortSec) {
                        if (currP.getMovePoints() < 2) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (2 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    } else {
                        if (currP.getMovePoints() < 4) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (4 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    }
                    break;
                case 6:
                    if (nCity.getAirport() != 3 && nCity.getAirport() != 5 && nCity.getAirport() != curairPortSec) {
                        ui.createDialog("You can not fly there!!\n Pick a different city.", 200, 150);
                    } else if (nCity.getAirport() == curairPortSec) {
                        if (currP.getMovePoints() < 2) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (2 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    } else {
                        if (currP.getMovePoints() < 4) {
                            ui.createDialog("You can not fly there!!\n NOT ENOUGH MOVE POINTS (4 REQUIRED)", 350, 150);
                        } else {
                            for (JourneyPlayer pl : currentGame.getPlayers()) {
                                if (pl.getPlayerNum() != currP.getPlayerNum()) {
                                    if (pl.getCurrentCity().equals(butName)) {
                                        ui.createDialog("Another player already at destination.\n Please choose a different route!", 250, 150);
                                    } else {
                                        currP.setMovePoints(currP.getMovePoints() - 2);
                                        ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                    }
                                } else {
                                    currP.setMovePoints(currP.getMovePoints() - 2);
                                    ui.fly(curC, nCity, currP, pieces, xCoords, yCoords, currentGame);
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }
}
