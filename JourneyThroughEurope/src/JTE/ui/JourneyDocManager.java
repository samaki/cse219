/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.ui;

import JTE.game.JourneyGameData;
import JTE.game.JourneyPlayer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;

/**
 *
 * @author Kwun Chan
 */
public class JourneyDocManager {

    private HTMLDocument moveDoc;
    private JourneyUI ui;

    private final String EMPTY_TEXT = "";

    // THESE ARE IDs IN THE STATS HTML FILE SO THAT WE CAN
    // GRAB THE NECESSARY ELEMENTS AND UPDATE THEM
    private final String PLAYER_1_MOVES = "move1";
    private final String PLAYER_2_MOVES = "move2";
    private final String PLAYER_3_MOVES = "move3";
    private final String PLAYER_4_MOVES = "move4";
    private final String PLAYER_5_MOVES = "move5";
    private final String PLAYER_6_MOVES = "move6";
    private final String PLAYER_1_ROLLS = "roll1";
    private final String PLAYER_2_ROLLS = "roll2";
    private final String PLAYER_3_ROLLS = "roll3";
    private final String PLAYER_4_ROLLS = "roll4";
    private final String PLAYER_5_ROLLS = "roll5";
    private final String PLAYER_6_ROLLS = "roll6";

    public JourneyDocManager(JourneyUI uii) {
        ui = uii;
    }

    public void setMoveDoc(HTMLDocument initMoveDoc) {
        moveDoc = initMoveDoc;
    }

    public void addMovesToDoc(int playerNum) {
        JourneyGameData game = ui.getGSM().getGame();
        String moves = "";
        String ele = "";

        JourneyPlayer p = game.getPlayers().get((playerNum - 1));
        ArrayList<String> move = p.getTotalMoves();
        for (String move1 : move) {
            String r = move1 + "," + " ";
            moves = r;
        }
        //System.out.print(moves);
        switch ((playerNum - 1)) {
            case 0:
                ele = PLAYER_1_MOVES;
                break;
            case 1:
                ele = PLAYER_2_MOVES;
                break;
            case 2:
                ele = PLAYER_3_MOVES;
                break;
            case 3:
                ele = PLAYER_4_MOVES;
                break;
            case 4:
                ele = PLAYER_5_MOVES;
                break;
            case 5:
                ele = PLAYER_6_MOVES;
                break;
        }
        try {
            Element movesElement = moveDoc.getElement(ele);
            moveDoc.insertBeforeEnd(movesElement, EMPTY_TEXT + moves);
            //moveDoc.insertAfterEnd(movesElement, EMPTY_TEXT + moves);
        } catch (BadLocationException | IOException ex) {
            Logger.getLogger(JourneyDocManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addRollsToDoc(int playerNum) {
        JourneyGameData game = ui.getGSM().getGame();
        String rol = "";
        String ele = "";

        JourneyPlayer p = game.getPlayers().get((playerNum - 1));
        ArrayList<Integer> rolls = p.getRolls();
        for (Integer roll1 : rolls) {
            int roll = roll1;
            String r = roll + ", " + " ";
            rol = rol.concat(r);
        }
        //System.out.println(rol);
        switch ((playerNum - 1)) {
            case 0:
                ele = PLAYER_1_ROLLS;
                break;
            case 1:
                ele = PLAYER_2_ROLLS;
                break;
            case 2:
                ele = PLAYER_3_ROLLS;
                break;
            case 3:
                ele = PLAYER_4_ROLLS;
                break;
            case 4:
                ele = PLAYER_5_ROLLS;
                break;
            case 5:
                ele = PLAYER_6_ROLLS;
                break;
        }
        try {
            Element rollsElement = moveDoc.getElement(ele);
            moveDoc.setInnerHTML(rollsElement, rol);
        } catch (BadLocationException ex) {
            Logger.getLogger(JourneyDocManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JourneyDocManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
