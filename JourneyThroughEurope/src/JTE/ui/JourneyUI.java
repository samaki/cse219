/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.ui;

import JTE.file.HTMLLoader;
import JTE.game.CardData;
import JTE.game.City;
import JTE.game.CityWrapper;
import JTE.game.JourneyGameData;
import JTE.game.JourneyGameStateManager;
import JTE.game.JourneyMap;
import JTE.game.JourneyPlayer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.text.html.HTMLDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import journeythrougheurope.JourneyThroughEurope.JourneyPropertyType;
import properties_manager.PropertiesManager;

/**
 *
 * @author Kwun Chan
 */
public class JourneyUI {

    private static class Delta {

        double X, Y;

        public Delta() {
        }
    }

    public enum JourneyUIState {

        SPLASH_SCREEN_STATE, PLAY_GAME_STATE, ABOUT_STATE, GAME_SETUP_STATE, MOVE_HISTORY_STATE, FLIGHT_PLAN_STATE
    }

    private Stage primaryStage;
    private BorderPane mainPane;
    private Insets marginlessInsets;
    private int paneWidth;
    private int paneHeight;

    private ImageView splashImageView;
    private StackPane splashScreenPane;
    private Label splashScreenImageLabel;
    private VBox start;
    private Button aboutB;
    private Button quitB;

    private JEditorPane about;
    private StackPane aboutPane;

    private BorderPane gameSetup;
    private FlowPane gameSetupMain;
    private HBox gameSetupT;
    private final ObservableList<String> options = FXCollections.observableArrayList(
            "1", "2", "3", "4", "5", "6");
    private ComboBox numP;
    private ArrayList<BorderPane> panes;
    boolean fromSplash = true;

    private HBox toolbar;
    private BorderPane gamePlayMain;
    private StackPane mainS;
    private StackPane cards;
    private VBox gameS;
    private Button die;
    private boolean rol;
    private Label diceP;
    private ArrayList<ImageView> pieces;

    private ArrayList<Integer> pieceXCoords;
    private ArrayList<Integer> pieceYCoords;
    private Line line1;
    private Label turn;
    private JEditorPane currentMoves;
    private StackPane currentMovePane;
    private JScrollPane currentMoveDisplay;
    
    private Random r1,r2,r3;
    private int seed,seed2,seed3;
    
    private AnchorPane map;
    private ScrollPane scorl;
    private Label cityName;
    private StackPane stack;
    private Button flight;
    private Button endTurn;

    private ScrollPane flightS;
    private AnchorPane flightA;

    private final String ImgPath = "file:images/";
    private final String dataPath = "file:data/";
    private final String buttonStyle = "-fx-padding: 8 15 15 15;\n"
            + "		-fx-background-radius: 5;\n"
            + "		-fx-background-color:\n"
            + "			linear-gradient(#f4b400, #0c7dc1),\n"
            + "			linear-gradient(from 0% 0% to 15% 40%, rgba(243, 192, 17, 0.9), rgba(255,255,255,0));\n"
            + "		-fx-effect: dropshadow(gaussian, rgba(0,0,0,1), 4,0,0,1);\n"
            + "		-fx-font-weight:bold;\n"
            + "		-fx-font-size: 1.50em;";
    private final String buttonStyle2 = "-fx-background-color:\n"
            + "		linear-gradient(#f4b400, #0c7dc1),\n"
            + "		linear-gradient(from 0% 0% to 80% 80%, rgba(243, 192, 17, 0.9), rgba(255,255,255,0));\n"
            + "-fx-font-weight:bold;\n" + "-fx-font-size:0.9em";
    private JourneyEventHandler eventHandler;
    private JourneyGameStateManager gsm;
    private JourneyDocManager doc;
    private JourneyMap jmap;

    public JourneyUI() {
        eventHandler = new JourneyEventHandler(this);
        gsm = new JourneyGameStateManager(this);
        doc = new JourneyDocManager(this);
        jmap = new JourneyMap();
        initMainPane();
        createButtons();
        initSplashScreen();
        initAboutScreen();
        initGameSetupScreen();
    }

    public void SetStage(Stage stage) {
        primaryStage = stage;
    }

    public BorderPane GetMainPane() {
        return this.mainPane;
    }

    public ArrayList<BorderPane> getGameSetupPanes() {
        return this.panes;
    }

    public JourneyGameStateManager getGSM() {
        return this.gsm;
    }

    public JourneyMap getMap() {
        return this.jmap;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setRoll(boolean t) {
        rol = t;
    }

    public String getButStyle() {
        return buttonStyle2;
    }

    public Button getDie() {
        return die;
    }

    public Button getFlightButton() {
        return flight;
    }

    public JourneyDocManager getDocManager() {
        return this.doc;
    }

    public void initMainPane() {
        marginlessInsets = new Insets(5, 5, 5, 6);
        mainPane = new BorderPane();

        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        paneWidth = Integer.parseInt(prop.getProperty(JourneyPropertyType.WINDOW_WIDTH));
        paneHeight = Integer.parseInt(prop.getProperty(JourneyPropertyType.WINDOW_HEIGHT));
        mainPane.resize(paneWidth, paneHeight);
        mainPane.setPadding(marginlessInsets);
        mainPane.setStyle("-fx-background-color: #EBE0CC");
    }

    public void initSplashScreen() {
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        String splashImagePath = prop.getProperty(JourneyPropertyType.SPLASH_SCREEN_IMAGE_NAME);
        splashScreenPane = new StackPane();

        Image splashScreenImage = loadImage(splashImagePath);
        splashImageView = new ImageView(splashScreenImage);

        splashScreenImageLabel = new Label();
        splashScreenImageLabel.setGraphic(splashImageView);
        splashScreenImageLabel.setLayoutX(-45);//
        splashScreenPane.getChildren().add(splashScreenImageLabel);

        start = new VBox();
        Button s = new Button("Start Game");
        s.setStyle(buttonStyle);
        s.setOnAction((ActionEvent event) -> {
            eventHandler.changeWorkspace(JourneyUIState.GAME_SETUP_STATE);
        });

        Button load = new Button("Load Game");
        load.setStyle(buttonStyle);

        //s.getStyleClass().add(this.getClass().getResource("button.css").toExternalForm());
        start.setSpacing(10.0);
        start.getChildren().add(s);
        start.getChildren().add(load);
        start.getChildren().add(aboutB);
        start.getChildren().add(quitB);
        start.setAlignment(Pos.CENTER_LEFT);
        splashScreenPane.getChildren().add(start);
        mainPane.setCenter(splashScreenPane);
    }

    public void initAboutScreen() {
        about = new JEditorPane();
        about.setEditable(false);
        SwingNode swingNode = new SwingNode();
        swingNode.setContent(about);

        about.setContentType("text/html");
        Button home = new Button("Back");
        home.setStyle(buttonStyle);
        home.setAlignment(Pos.BASELINE_CENTER);
        home.setOnAction((ActionEvent event) -> {
            if (fromSplash) {
                eventHandler.changeWorkspace(JourneyUIState.SPLASH_SCREEN_STATE);
            } else {
                eventHandler.changeWorkspace(JourneyUIState.PLAY_GAME_STATE);
            }
        });

        aboutPane = new StackPane();
        aboutPane.getChildren().add(swingNode);
        aboutPane.getChildren().add(home);

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String fileName = props.getProperty(JourneyPropertyType.ABOUT_FILE_NAME);
        try {
            // LOAD THE HTML INTO THE EDITOR PANE
            String fileHTML = HTMLLoader.loadTextFile(fileName);
            about.setText(fileHTML);
        } catch (IOException ioe) {

        }
    }

    public void initGameSetupScreen() {
        gameSetup = new BorderPane();
        gameSetupMain = new FlowPane();
        numP = new ComboBox(options);

        Label cL = new Label();
        cL.setText("Number of Players:");
        cL.setStyle("-fx-font-weight:bold; \n" + "-fx-font-size: 1.30em;");

        panes = new ArrayList<>();
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        String flagPath = "";
        numP.setValue(options.get(0));

        for (int i = 0; i < 6; i++) {
            switch (i) {
                case 0:
                    flagPath = prop.getProperty(JourneyPropertyType.PLAYER1_FLAG_NAME);
                    break;
                case 1:
                    flagPath = prop.getProperty(JourneyPropertyType.PLAYER2_FLAG_NAME);
                    break;
                case 2:
                    flagPath = prop.getProperty(JourneyPropertyType.PLAYER3_FLAG_NAME);
                    break;
                case 3:
                    flagPath = prop.getProperty(JourneyPropertyType.PLAYER4_FLAG_NAME);
                    break;
                case 4:
                    flagPath = prop.getProperty(JourneyPropertyType.PLAYER5_FLAG_NAME);
                    break;
                case 5:
                    flagPath = prop.getProperty(JourneyPropertyType.PLAYER6_FLAG_NAME);
                    break;
            }

            Image flagImage = loadImage(flagPath);
            ImageView flagImageView = new ImageView(flagImage);
            flagImageView.setFitHeight(150);
            flagImageView.setFitWidth(100);
            flagImageView.setPreserveRatio(true);

            BorderPane s = new BorderPane();
            ToggleGroup group = new ToggleGroup();
            RadioButton button1 = new RadioButton("Player");
            button1.setToggleGroup(group);
            button1.setSelected(true);
            RadioButton button2 = new RadioButton("Computer");
            button2.setToggleGroup(group);
            VBox radio = new VBox();
            radio.setAlignment(Pos.CENTER_LEFT);
            radio.setSpacing(15.0);
            radio.getChildren().addAll(button1, button2);
            radio.setPrefWidth(75);

            VBox namePart = new VBox();
            Label p = new Label();
            p.setText("Player Name:");
            TextField name = new TextField("Player" + " " + (i + 1));
            namePart.getChildren().addAll(p, name);
            namePart.setAlignment(Pos.CENTER);
            name.setPrefWidth(100);

            s.setCenter(radio);
            s.setLeft(flagImageView);
            s.setRight(namePart);
            s.setStyle("-fx-background-color:#E6B800");
            s.setPrefSize(313, 270);
            panes.add(s);
        }

        r1 = new Random(14647741);
        r2 = new Random(356841);
        r3 = new Random(4139874);

        Button go = new Button("Go!");
        go.setStyle(buttonStyle2);
        go.setOnAction((ActionEvent event) -> {
            String select = (String) numP.getSelectionModel().getSelectedItem();
            int selectedNum = Integer.parseInt(select.trim());
            seed = r1.nextInt();
            seed2 = r2.nextInt();
            seed3 = r3.nextInt();
            eventHandler.startGame(selectedNum, JourneyUIState.PLAY_GAME_STATE, seed, seed2, seed3);
        });

        gameSetupT = new HBox();
        gameSetupT.setSpacing(5.0);
        gameSetupT.getChildren().add(cL);
        gameSetupT.getChildren().add(numP);
        gameSetupT.getChildren().add(go);

        gameSetupMain.getChildren().add(panes.get(0));
        for (int i = 1; i < 6; i++) {
            gameSetupMain.getChildren().add(panes.get(i));
            gameSetupMain.getChildren().get(i).setVisible(false);
        }

        numP.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String select = (String) numP.getSelectionModel().getSelectedItem();
                int selectedNum = Integer.parseInt(select.trim());
                for (int i = 0; i < selectedNum; i++) {
                    gameSetupMain.getChildren().get(i).setVisible(true);
                    if (selectedNum < 6) {
                        for (int z = selectedNum; z < 6; z++) {
                            gameSetupMain.getChildren().get(z).setVisible(false);
                        }
                    }
                }
            }
        });

        gameSetup.setStyle("-fx-background-color: #EBE0CC");
        gameSetup.setTop(gameSetupT);
        gameSetup.setCenter(gameSetupMain);
    }

    public void initGamePlayScreen(int seed) {
        initToolbar();
        initCurrentHistoryScreen();
        initFlightScreen();

        pieces = new ArrayList<>();
        pieceXCoords = new ArrayList<>();
        pieceYCoords = new ArrayList<>();
        gamePlayMain = new BorderPane();
        primaryStage.setHeight(paneHeight + 165);
        primaryStage.setWidth(paneWidth + 150);
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        String map1Path = prop.getProperty(JourneyPropertyType.MAP_FILE_NAME);

        Image map1Image = loadImage(map1Path);
        ImageView map1View = new ImageView(map1Image);

        Label map1 = new Label();
        map1.setGraphic(map1View);

        stack = new StackPane();
        cards = new StackPane();
        map = new AnchorPane();
        map.getChildren().add(map1);
        scorl = new ScrollPane();
        scorl.setContent(map);
        scorl.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scorl.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        gameS = new VBox();
        die = new Button();
        String diePath = prop.getProperty(JourneyPropertyType.DIE1_FILE);
        Image dieImage = loadImage(diePath);
        ImageView dieView = new ImageView(dieImage);
        die.setGraphic(dieView);
        Random rand = new Random(seed);
        diceP = new Label("Dice Points: ");

        die.setOnAction((ActionEvent event) -> {
            int roll = rand.nextInt(6) + 1;
            String diePathh = prop.getProperty(JourneyPropertyType.DIE1_FILE);
            Image dieImagee = loadImage(diePathh);
            ImageView dieVieww = new ImageView(dieImagee);

            switch (roll) {
                case 1:
                    diePathh = prop.getProperty(JourneyPropertyType.DIE1_FILE);
                    dieImagee = loadImage(diePathh);
                    dieVieww = new ImageView(dieImagee);
                    die.setGraphic(dieVieww);
                    break;
                case 2:
                    diePathh = prop.getProperty(JourneyPropertyType.DIE2_FILE);
                    dieImagee = loadImage(diePathh);
                    dieVieww = new ImageView(dieImagee);
                    die.setGraphic(dieVieww);
                    break;
                case 3:
                    diePathh = prop.getProperty(JourneyPropertyType.DIE3_FILE);
                    dieImagee = loadImage(diePathh);
                    dieVieww = new ImageView(dieImagee);
                    die.setGraphic(dieVieww);
                    break;
                case 4:
                    diePathh = prop.getProperty(JourneyPropertyType.DIE4_FILE);
                    dieImagee = loadImage(diePathh);
                    dieVieww = new ImageView(dieImagee);
                    die.setGraphic(dieVieww);
                    break;
                case 5:
                    diePathh = prop.getProperty(JourneyPropertyType.DIE5_FILE);
                    dieImagee = loadImage(diePathh);
                    dieVieww = new ImageView(dieImagee);
                    die.setGraphic(dieVieww);
                    break;
                case 6:
                    diePathh = prop.getProperty(JourneyPropertyType.DIE6_FILE);
                    dieImagee = loadImage(diePathh);
                    dieVieww = new ImageView(dieImagee);
                    die.setGraphic(dieVieww);
                    break;
            }
            diceP.setText("Dice Points: " + roll);
            rol = true;
            eventHandler.playerMoves(roll);
            die.setDisable(true);
        });
        die.setStyle(buttonStyle2);

        HBox gridTop = new HBox();
        Button sect1 = new Button();
        String sect1Path = prop.getProperty(JourneyPropertyType.SECTOR1_FILE);
        Image sect1Image = loadImage(sect1Path);
        ImageView sect1View = new ImageView(sect1Image);
        sect1.setGraphic(sect1View);
        sect1.setOnAction((ActionEvent event) -> {
            scorl.setHvalue(0);
            scorl.setVvalue(0.2);
        });
        sect1.setStyle(buttonStyle2);

        Button sect2 = new Button();
        String sect2Path = prop.getProperty(JourneyPropertyType.SECTOR2_FILE);
        Image sect2Image = loadImage(sect2Path);
        ImageView sect2View = new ImageView(sect2Image);
        sect2.setGraphic(sect2View);
        sect2.setOnAction((ActionEvent event) -> {
            scorl.setHvalue(1);
            scorl.setVvalue(0.05);
        });
        sect2.setStyle(buttonStyle2);

        gridTop.getChildren().addAll(sect1, sect2);
        HBox gridBot = new HBox();
        Button sect3 = new Button();
        String sect3Path = prop.getProperty(JourneyPropertyType.SECTOR3_FILE);
        Image sect3Image = loadImage(sect3Path);
        ImageView sect3View = new ImageView(sect3Image);
        sect3.setGraphic(sect3View);
        sect3.setOnAction((ActionEvent event) -> {
            scorl.setHvalue(0);
            scorl.setVvalue(.8);
        });
        sect3.setStyle(buttonStyle2);

        Button sect4 = new Button();
        String sect4Path = prop.getProperty(JourneyPropertyType.SECTOR4_FILE);
        Image sect4Image = loadImage(sect4Path);
        ImageView sect4View = new ImageView(sect4Image);
        sect4.setGraphic(sect4View);
        sect4.setOnAction((ActionEvent event) -> {
            scorl.setHvalue(1);
            scorl.setVvalue(0.8);
        });
        sect4.setStyle(buttonStyle2);

        endTurn = new Button();
        endTurn.setText("End Turn");
        endTurn.setOnAction((ActionEvent event) -> {
            diceP.setText("Dice Points: 0");
            eventHandler.endTurnPrematurely();
        });
        endTurn.setStyle(buttonStyle2);
        gridBot.getChildren().addAll(sect3, sect4);

        VBox sectors = new VBox();
        sectors.getChildren().addAll(gridTop, gridBot);
        gameS.setSpacing(20);
        turn = new Label();

        flight = new Button("Open Flight Plan");
        flight.setOnAction((ActionEvent event) -> {
            if (flight.getText().equals("Open Flight Plan")) {
                changeWorkspace(JourneyUIState.FLIGHT_PLAN_STATE);
                flight.setText("Back");
            } else {
                scorl.toFront();
                flight.setText("Open Flight Plan");
            }
        });

        flight.setStyle(buttonStyle2);
        stack.getChildren().add(flightS);
        stack.getChildren().add(scorl);
        gameS.getChildren().addAll(die, diceP, sectors, flight, endTurn, turn);
        gamePlayMain.setLeft(cards);
        gamePlayMain.setCenter(stack);
        gamePlayMain.setRight(gameS);

        mainPane.setTop(toolbar);
        mainPane.setCenter(gamePlayMain);
    }

    public void initFlightScreen() {
        flightS = new ScrollPane();
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        String flightPath = prop.getProperty(JourneyPropertyType.FLIGHT_FILE);
        Image flightImage = loadImage(flightPath);
        ImageView flightView = new ImageView(flightImage);
        flightA = new AnchorPane();
        flightA.getChildren().add(flightView);
        flightS.setContent(flightA);
    }

    public void createFlightButtons() {
        ArrayList<City> cit = jmap.getCities();
        for (City c : cit) {
            if (c.getFlightX() >= 0) {
                Button city = new Button();
                city.setTooltip(new Tooltip(c.getCityName()));
                city.resize(1, 1);
                Circle circle = new Circle(10);
                city.setShape(circle);
                city.relocate(c.getFlightX() - 2, c.getFlightY() - 5);
                city.setOnAction((ActionEvent evt) -> {
                    eventHandler.handleFlight(c.getCityName(), pieces, pieceXCoords, pieceYCoords);
                });
                flightA.getChildren().add(city);
            }
        }
    }

    public void initToolbar() {
        toolbar = new HBox();
        Button saveGame = new Button("Save Game");
        saveGame.setStyle(buttonStyle);

        Button openHist = new Button("Open Current History");
        openHist.setStyle(buttonStyle);
        openHist.setOnAction((ActionEvent event) -> {
            eventHandler.changeWorkspace(JourneyUIState.MOVE_HISTORY_STATE);
        });

        toolbar.setSpacing(10.0);
        toolbar.setAlignment(Pos.CENTER);
        toolbar.getChildren().addAll(saveGame, openHist, aboutB, quitB);
    }

    public void createButtons() {
        aboutB = new Button("About");
        aboutB.setStyle(buttonStyle);
        aboutB.setOnAction((ActionEvent event) -> {
            eventHandler.changeWorkspace(JourneyUIState.ABOUT_STATE);
        });

        quitB = new Button("Quit Game");
        quitB.setStyle(buttonStyle);
        quitB.setOnAction((ActionEvent event) -> {
            // TODO Auto-generated method stub
            // System.out.println(lang);
            eventHandler.respondToQuitRequest();
        });
    }

    public void initCurrentHistoryScreen() {
        currentMoves = new JEditorPane();
        currentMoves.setEditable(false);
        currentMoves.setContentType("text/html");

        SwingNode swingNode = new SwingNode();
        currentMoveDisplay = new JScrollPane(currentMoves);
        swingNode.setContent(currentMoveDisplay);

        Button back = new Button("Back");
        back.setStyle(buttonStyle);
        back.setAlignment(Pos.TOP_CENTER);

        back.setOnAction((ActionEvent event) -> {
            eventHandler.changeWorkspace(JourneyUIState.PLAY_GAME_STATE);
        });

        ScrollPane stats = new ScrollPane();
        stats.setContent(swingNode);
        stats.setFitToHeight(true);
        stats.setFitToWidth(true);
        currentMovePane = new StackPane();
        currentMovePane.getChildren().add(stats);
        currentMovePane.getChildren().add(back);
        StackPane.setMargin(back, new Insets(-590, 0, 0, 0));

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String fileName = props.getProperty(JourneyPropertyType.MOVE_FILE_NAME);
        try {
            // LOAD THE HTML INTO THE EDITOR PANE
            String fileHTML = HTMLLoader.loadTextFile(fileName);
            currentMoves.setText(fileHTML);
        } catch (IOException ioe) {

        }

        HTMLDocument moveDoc = (HTMLDocument) currentMoves.getDocument();
        doc.setMoveDoc(moveDoc);

    }

    public Image loadImage(String imageName) {
        Image img = new Image(ImgPath + imageName);
        return img;
    }

    public void createCityButtons() {
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        String cityPath = prop.getProperty(JourneyPropertyType.CITIES_FILE);
        cityPath = prop.getProperty(JourneyPropertyType.DATA_PATH) + cityPath;
        File file = new File(cityPath);

        try {
            JAXBContext jc = JAXBContext.newInstance(CityWrapper.class);
            Unmarshaller unmarsh = jc.createUnmarshaller();

            CityWrapper wrapper = (CityWrapper) unmarsh.unmarshal(file);

            jmap.setCities(wrapper.getCities());

        } catch (Exception e) { // catches ANY exception
            e.printStackTrace();
        }
        ArrayList<City> cit = jmap.getCities();
        for (City c : cit) {
            Button city = new Button();
            city.setTooltip(new Tooltip(c.getCityName()));
            city.resize(1, 1);
            Circle circle = new Circle(5);
            //city.setOpacity(0f);
            city.setShape(circle);
            if (c.getQuarter() == 1) {
                city.relocate((c.getXCoord() - 30) / 3, (c.getYCoord() - 30) / 3);
            } else if (c.getQuarter() == 2) {
                city.relocate((c.getXCoord() + 1950) / 3, (c.getYCoord() - 38) / 3);
            } else if (c.getQuarter() == 3) {
                city.relocate((c.getXCoord() - 35) / 3, (c.getYCoord() + 2495) / 3);
            } else if (c.getQuarter() == 4) {
                city.relocate((c.getXCoord() + 1932) / 3, (c.getYCoord() + 2500) / 3);
            }

            city.setOnAction((ActionEvent event) -> {
                if (!rol) {
                    createDialog("You have not rolled the die.", 300, 50);
                } else {
                    eventHandler.movePlayer(c.getCityName(), pieces, pieceXCoords, pieceYCoords);
                }
            });
            map.getChildren().add(city);
        }
    }

    public void createCardList(int playerNum) {
        AnchorPane t = new AnchorPane();
        ArrayList<CardData> playerCards = gsm.getGame().getPlayers().get(playerNum).getCards();
        int prev = 200;

        for (int i = 0; i < playerCards.size(); i++) {

            Image cardImg = playerCards.get(i).getFront();
            ImageView cardImg2 = new ImageView(cardImg);
            cardImg2.setFitHeight(300);
            cardImg2.setFitWidth(200);
            cardImg2.setScaleX(2);
            cardImg2.setScaleY(2);
            if (i == 0) {
                cardImg2.relocate(0, prev);
            } else {
                prev += 85;
                cardImg2.relocate(0, prev);
            }
            t.getChildren().add(cardImg2);
            TranslateTransition tt = new TranslateTransition(Duration.millis(2000), cardImg2);
            tt.setFromY(prev);
            tt.setToY(prev - 400);
            ScaleTransition st = new ScaleTransition(Duration.millis(2000), cardImg2);
            st.setToX(1f);
            st.setToY(1f);
            ParallelTransition parallelTransition = new ParallelTransition();
            parallelTransition.getChildren().addAll(tt, st);
            parallelTransition.play();
        }
        cards.getChildren().add(t);
        initSpotOnMap(playerCards.get(0), playerNum);
    }

    public void initSpotOnMap(CardData initCard, int playerNum) {
        String name = initCard.getName();
        gsm.getGame().getPlayers().get(playerNum).setCurrentCity(name);
        //System.out.println(name);
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        String flagPath = " ";
        String piecePath = " ";
        int s = playerNum + 2;
        int quarter = 0;

        switch (playerNum) {
            case 0:
                flagPath = prop.getProperty(JourneyPropertyType.PLAYER1_FLAG_NAME);
                piecePath = prop.getProperty(JourneyPropertyType.PLAYER1_PIECE);
                break;
            case 1:
                flagPath = prop.getProperty(JourneyPropertyType.PLAYER2_FLAG_NAME);
                piecePath = prop.getProperty(JourneyPropertyType.PLAYER2_PIECE);
                break;
            case 2:
                flagPath = prop.getProperty(JourneyPropertyType.PLAYER3_FLAG_NAME);
                piecePath = prop.getProperty(JourneyPropertyType.PLAYER3_PIECE);
                break;
            case 3:
                flagPath = prop.getProperty(JourneyPropertyType.PLAYER4_FLAG_NAME);
                piecePath = prop.getProperty(JourneyPropertyType.PLAYER4_PIECE);
                break;
            case 4:
                flagPath = prop.getProperty(JourneyPropertyType.PLAYER5_FLAG_NAME);
                piecePath = prop.getProperty(JourneyPropertyType.PLAYER5_PIECE);
                break;
            case 5:
                flagPath = prop.getProperty(JourneyPropertyType.PLAYER6_FLAG_NAME);
                piecePath = prop.getProperty(JourneyPropertyType.PLAYER6_PIECE);
                break;
        }
        Image flagImg = loadImage(flagPath);
        ImageView flagView = new ImageView(flagImg);
        Image pieceImg = loadImage(piecePath);
        ImageView pieceView = new ImageView(pieceImg);
        /*final Delta drag = new Delta();
         pieceView.setOnMouseClicked((MouseEvent t) -> {
         // drag.X = pieceView.getLayoutX() - t.getSceneX();
         // drag.Y = pieceView.getLayoutY() - t.getSceneY();
         System.out.println(drag.X);
         System.out.println(drag.Y);
         pieceView.setCursor(Cursor.MOVE);
         });
         pieceView.setOnMouseReleased((MouseEvent mouseEvent) -> {
         });
         pieceView.setOnDragDetected((MouseEvent mouseEvent) -> {
         drag.X = pieceView.getLayoutX() - mouseEvent.getSceneX();
         drag.Y = pieceView.getLayoutY() - mouseEvent.getSceneY();
         pieceView.startFullDrag();
         pieceView.setOnMouseDragEntered((MouseEvent mouseEvent1) -> {
         pieceView.setLayoutX(mouseEvent1.getSceneX() + drag.X);
         pieceView.setLayoutY(mouseEvent1.getSceneY() + drag.Y);
         });
         });*/

        int xCoord = 0;
        int yCoord = 0;

        for (City c : jmap.getCities()) {
            if (name.equals(c.getCityName())) {
                if (c.getQuarter() == 1) {
                    xCoord = ((c.getXCoord() - 30) / 3);
                    yCoord = ((c.getYCoord() - 30) / 3);
                    quarter = 1;
                } else if (c.getQuarter() == 2) {
                    xCoord = ((c.getXCoord() + 1950) / 3);
                    yCoord = ((c.getYCoord() - 38) / 3);
                    quarter = 2;
                } else if (c.getQuarter() == 3) {
                    xCoord = ((c.getXCoord() - 35) / 3);
                    yCoord = ((c.getYCoord() + 2495) / 3);
                    quarter = 3;
                } else if (c.getQuarter() == 4) {
                    xCoord = ((c.getXCoord() + 1932) / 3);
                    yCoord = ((c.getYCoord() + 2500) / 3);
                    quarter = 4;
                }
            }
        }
        pieces.add(pieceView);
        pieceXCoords.add(xCoord - 10);
        pieceYCoords.add(yCoord - 25);
        flagView.relocate(xCoord - 20, yCoord - 25);
        flagView.setFitHeight(50);
        flagView.setFitWidth(35);
        flagView.setScaleX(5);
        flagView.setScaleY(5);
        pieceView.relocate(xCoord - 10, yCoord - 25);
        pieceView.setFitHeight(50);
        pieceView.setFitWidth(25);
        pieceView.setScaleX(5);
        pieceView.setScaleY(5);

        switch (quarter) {
            case 1:
                scorl.setHvalue(0);
                scorl.setVvalue(0.2);
                break;
            case 2:
                scorl.setHvalue(1);
                scorl.setVvalue(0.2);
                break;
            case 3:
                scorl.setHvalue(0);
                scorl.setVvalue(.8);
                break;
            case 4:
                scorl.setHvalue(1);
                scorl.setVvalue(.8);
                break;
        }

        map.getChildren().addAll(flagView, pieceView);

        Timeline timeline = new Timeline();
        KeyValue kv = new KeyValue(pieceView.scaleXProperty(), 1);
        KeyValue kv2 = new KeyValue(pieceView.scaleYProperty(), 1);
        KeyValue kv3 = new KeyValue(flagView.scaleXProperty(), 1);
        KeyValue kv4 = new KeyValue(flagView.scaleYProperty(), 1);
        EventHandler onFinished = (EventHandler<ActionEvent>) (ActionEvent e) -> {
            if (s <= gsm.getGame().getPlayers().size()) {
                //System.out.println(cards.getChildren().size());
                eventHandler.initPlayers(s);
            } else {
                eventHandler.startPlayerTurn(1);
            }
        };
        KeyFrame kf = new KeyFrame(Duration.millis(2000), onFinished, kv, kv2, kv3, kv4);
        timeline.getKeyFrames().add(kf);
        timeline.setCycleCount(1);
        timeline.play();
    }

    public void setupTurn(int player) {
        die.setDisable(false);
        JourneyPlayer play = gsm.getGame().getPlayers().get(player - 1);
        String name = play.getCurrentCity();
        City curC = null;
        for (City c : jmap.getCities()) {
            if (name.equals(c.getCityName())) {
                curC = c;
            }
        }

        if (play.getHarb()) {
            flight.setDisable(true);
        } else {
            if (curC.getAirport() > 0) {
                flight.setDisable(false);
            } else {
                flight.setDisable(true);
            }
        }
        int q = curC.getQuarter();

        switch (q) {
            case 1:
                scorl.setHvalue(0);
                scorl.setVvalue(0.2);
                break;
            case 2:
                scorl.setHvalue(1);
                scorl.setVvalue(0.2);
                break;
            case 3:
                scorl.setHvalue(0);
                scorl.setVvalue(.8);
                break;
            case 4:
                scorl.setHvalue(1);
                scorl.setVvalue(.8);
                break;
        }

        cards.getChildren().get(player - 1).setVisible(true);
        for (int i = 0; i < cards.getChildren().size(); i++) {
            if (i != player - 1) {
                cards.getChildren().get(i).setVisible(false);
            }
        }

        turn.setText("Player" + player + " Turn!!!!");
        drawRedLine(curC);
    }

    public void drawRedLine(City c) {
        ArrayList<String> land = c.landNeighbors;
        ArrayList<String> sea = c.seaNeighbors;
        int xCoord = 0, yCoord = 0;
        int quarter = 0;
        int xCoord2 = 0, yCoord2 = 0, quarter2 = 0;
        City c2 = null;
        for (int i = 0; i < c.landNeighbors.size(); i++) {
            String cc = c.landNeighbors.get(i);
            for (City ci : jmap.getCities()) {
                if (cc.equals(ci.getCityName())) {
                    c2 = ci;
                }
            }

            if (c.getQuarter() == 1) {
                xCoord = ((c.getXCoord() - 30) / 3);
                yCoord = ((c.getYCoord() - 30) / 3);
                quarter = 1;
            } else if (c.getQuarter() == 2) {
                xCoord = ((c.getXCoord() + 1960) / 3);
                yCoord = ((c.getYCoord() - 35) / 3);
                quarter = 2;
            } else if (c.getQuarter() == 3) {
                xCoord = ((c.getXCoord() - 25) / 3);
                yCoord = ((c.getYCoord() + 2510) / 3);
                quarter = 3;
            } else if (c.getQuarter() == 4) {
                xCoord = ((c.getXCoord() + 1932) / 3);
                yCoord = ((c.getYCoord() + 2500) / 3);
                quarter = 4;
            }

            if (c2.getQuarter() == 1) {
                xCoord2 = ((c2.getXCoord() - 30) / 3);
                yCoord2 = ((c2.getYCoord() - 30) / 3);
                quarter2 = 1;
            } else if (c2.getQuarter() == 2) {
                xCoord2 = ((c2.getXCoord() + 1960) / 3);
                yCoord2 = ((c2.getYCoord() - 35) / 3);
                quarter2 = 2;
            } else if (c2.getQuarter() == 3) {
                xCoord2 = ((c2.getXCoord() - 15) / 3);
                yCoord2 = ((c2.getYCoord() + 2530) / 3);
                quarter2 = 3;
            } else if (c2.getQuarter() == 4) {
                xCoord2 = ((c2.getXCoord() + 1932) / 3);
                yCoord2 = ((c2.getYCoord() + 2500) / 3);
                quarter2 = 4;
            }
            Line line1 = new Line((xCoord + 5), (yCoord + 7), (xCoord2 + 5), (yCoord2 + 7));
            line1.setStrokeWidth(3);
            line1.setStroke(Color.RED);
            map.getChildren().add(line1);
        }

        for (int i = 0; i < c.seaNeighbors.size(); i++) {
            String cc = c.seaNeighbors.get(i);
            for (City ci : jmap.getCities()) {
                if (cc.equals(ci.getCityName())) {
                    c2 = ci;
                }
            }

            if (c.getQuarter() == 1) {
                xCoord = ((c.getXCoord() - 30) / 3);
                yCoord = ((c.getYCoord() - 30) / 3);
                quarter = 1;
            } else if (c.getQuarter() == 2) {
                xCoord = ((c.getXCoord() + 1960) / 3);
                yCoord = ((c.getYCoord() - 35) / 3);
                quarter = 2;
            } else if (c.getQuarter() == 3) {
                xCoord = ((c.getXCoord() - 35) / 3);
                yCoord = ((c.getYCoord() + 2495) / 3);
                quarter = 3;
            } else if (c.getQuarter() == 4) {
                xCoord = ((c.getXCoord() + 1932) / 3);
                yCoord = ((c.getYCoord() + 2500) / 3);
                quarter = 4;
            }

            if (c2.getQuarter() == 1) {
                xCoord2 = ((c2.getXCoord() - 30) / 3);
                yCoord2 = ((c2.getYCoord() - 30) / 3);
                quarter2 = 1;
            } else if (c2.getQuarter() == 2) {
                xCoord2 = ((c2.getXCoord() + 1960) / 3);
                yCoord2 = ((c2.getYCoord() - 35) / 3);
                quarter2 = 2;
            } else if (c2.getQuarter() == 3) {
                xCoord2 = ((c2.getXCoord() - 35) / 3);
                yCoord2 = ((c2.getYCoord() + 2495) / 3);
                quarter2 = 3;
            } else if (c2.getQuarter() == 4) {
                xCoord2 = ((c2.getXCoord() + 1932) / 3);
                yCoord2 = ((c2.getYCoord() + 2500) / 3);
                quarter2 = 4;
            }
            Line line1 = new Line((xCoord + 5), (yCoord + 7), (xCoord2 + 5), (yCoord2 + 7));
            line1.setStrokeWidth(3);
            line1.setStroke(Color.RED);
            map.getChildren().add(line1);
        }
    }

    public void moveChar(String destCity, ImageView piece, int pxCoord, int pyCoord) {
        for (int i = 0; i < map.getChildren().size(); i++) {
            if (map.getChildren().get(i).getClass().getSimpleName().equals("Line")) {
                map.getChildren().remove(i);
                i--;
            }
        }

        JourneyPlayer currP = gsm.getGame().getPlayers().get(gsm.getGame().getPlayerTurn() - 1);
        diceP.setText("Dice Points:" + currP.getMovePoints());
        ArrayList<CardData> pCards = gsm.getGame().getPlayers().get(gsm.getGame().getPlayerTurn() - 1).getCards();
        ImageView p = piece;
        int pX = pxCoord;
        int pY = pyCoord;
        String dest = destCity;
        City destin = null;

        if (pCards.size() > 1) {
            for (int i = 1; i < pCards.size(); i++) {
                if (dest.equals(pCards.get(i).getName())) {
                    currP.setMovePoints(0);
                    createDialog(("You have arrived at one of your target cities: " + pCards.get(i).getName()), 300, 100);
                    diceP.setText("Dice Points: 0");
                    pCards.remove(i);
                    AnchorPane s = (AnchorPane) cards.getChildren().get(gsm.getGame().getPlayerTurn() - 1);
                    s.getChildren().remove(i);
                }
            }
        } else {
            if (dest.equals(pCards.get(0).getName())) {
                currP.setMovePoints(0);
                diceP.setText("Dice Points: 0");
                pCards.remove(0);
                AnchorPane s = (AnchorPane) cards.getChildren().get(gsm.getGame().getPlayerTurn() - 1);
                s.getChildren().remove(0);
                currP.setMovePoints(0);
                Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.initOwner(primaryStage);
                VBox noD = new VBox();
                Label ss = new Label("The game has been won!!! THE WINNER IS: PLAYER " + currP.getPlayerNum() + ", " + currP.getPlayerName());
                ss.setStyle("fx-font-weight:bold; \n" + "fx-font-size: 5em;");
                Button ok = new Button("Go Back to main menu");
                ok.setStyle(buttonStyle2);
                ok.setAlignment(Pos.CENTER);
                ok.setOnAction((ActionEvent et) -> {
                    mainPane.getChildren().remove(0, mainPane.getChildren().size());
                    mainPane.resize(paneWidth, paneHeight);
                    primaryStage.setHeight(paneHeight);
                    primaryStage.setWidth(paneWidth);
                    start.getChildren().addAll(aboutB, quitB);
                    changeWorkspace(JourneyUIState.SPLASH_SCREEN_STATE);
                    dialogStage.close();
                });
                Button quit = new Button("Quit Game");
                quit.setOnAction((ActionEvent a) -> {
                    System.exit(0);
                });
                quit.setAlignment(Pos.CENTER);
                quit.setStyle(buttonStyle2);
                HBox buts = new HBox();
                buts.setAlignment(Pos.CENTER);
                buts.setSpacing(5);
                buts.getChildren().addAll(ok, quit);
                noD.getChildren().addAll(ss, buts);
                noD.setSpacing(5);
                noD.setAlignment(Pos.CENTER);
                Scene scene = new Scene(noD, 500, 100);
                dialogStage.initStyle(StageStyle.UNDECORATED);
                dialogStage.setScene(scene);
                dialogStage.show();
            }
        }

        for (City c : jmap.getCities()) {
            if (dest.equals(c.getCityName())) {
                destin = c;
            }
        }
        if (destin.getAirport() > 0) {
            flight.setDisable(false);
            endTurn.setDisable(true);
        } else {
            flight.setDisable(true);
            endTurn.setDisable(false);
        }

        if (currP.getJustFlew()) {
            flight.setDisable(true);
        } else {
            flight.setDisable(false);
        }

        if (destin.getInfo() != null) {
            createDialog(destin.getInfo(), 750, 200);
        }

        int dX = destin.getXCoord();
        int dY = destin.getYCoord();

        if (destin.getQuarter() == 1) {
            dX = ((destin.getXCoord() - 30) / 3);
            dY = ((destin.getYCoord() - 30) / 3);
        } else if (destin.getQuarter() == 2) {
            dX = ((destin.getXCoord() + 1960) / 3);
            dY = ((destin.getYCoord() - 35) / 3);
        } else if (destin.getQuarter() == 3) {
            dX = ((destin.getXCoord() - 15) / 3);
            dY = ((destin.getYCoord() + 2530) / 3);
        } else if (destin.getQuarter() == 4) {
            dX = ((destin.getXCoord() + 1932) / 3);
            dY = ((destin.getYCoord() + 2500) / 3);
        }

        int difX = dX - pX - 10;
        int difY = dY - pY - 35;
        drawRedLine(destin);
        Timeline timeline = new Timeline();
        KeyValue kv = new KeyValue(p.xProperty(), difX);
        KeyValue kv2 = new KeyValue(p.yProperty(), difY);
        KeyFrame kf = new KeyFrame(Duration.millis(700), kv, kv2);
        timeline.getKeyFrames().add(kf);
        timeline.setCycleCount(1);
        timeline.setOnFinished((ActionEvent e) -> {
            if (currP.getMovePoints() <= 0) {
                if (currP.getRAgain()) {
                    die.setDisable(false);
                    currP.setRollAgain(false);
                    currP.setJustFlew(false);
                    flight.setDisable(false);
                    currP.setFly(false);
                } else {
                    eventHandler.endTurn();
                }
            }
        });
        timeline.play();

    }

    public void fly(City curC, City flyTo, JourneyPlayer p, ArrayList<ImageView> pieces, ArrayList<Integer> xCoords, ArrayList<Integer> yCoords, JourneyGameData currentGame) {
        ImageView p2 = pieces.get(p.getPlayerNum() - 1);
        int pxCoord = xCoords.get(p.getPlayerNum() - 1);
        int pyCoord = yCoords.get(p.getPlayerNum() - 1);
        if (curC.getCityName().equals(flyTo.getCityName())) {
            createDialog("You are currently at this city!!", 200, 100);
        } else {
            scorl.toFront();
            flight.setText("Open Flight Plan");
            p.setCurrentCity(flyTo.getCityName());
            p.setJustFlew(true);
            p.setFly(true);
            moveChar(flyTo.getCityName(), p2, pxCoord, pyCoord);
            flight.setDisable(true);
        }
    }

    public void createDialog(String state, int height, int width) {
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        VBox noD = new VBox();
        Label s = new Label(state);
        Button ok = new Button("Okay");
        ok.setStyle(buttonStyle2);
        ok.setOnAction((ActionEvent et) -> {
            dialogStage.close();
        });
        noD.getChildren().addAll(s, ok);
        noD.setSpacing(5);
        noD.setAlignment(Pos.CENTER);
        Scene scene = new Scene(noD, height, width);
        dialogStage.initStyle(StageStyle.UNDECORATED);
        dialogStage.setScene(scene);
        dialogStage.show();
    }

    public void changeWorkspace(JourneyUIState state) {
        switch (state) {
            case ABOUT_STATE:
                mainPane.setCenter(aboutPane);
                break;
            case SPLASH_SCREEN_STATE:
                mainPane.setCenter(splashScreenPane);
                fromSplash = true;
                break;
            case GAME_SETUP_STATE:
                gameSetup.resize(paneWidth, paneHeight);
                mainPane.setCenter(gameSetup);
                break;
            case PLAY_GAME_STATE:
                mainPane.setCenter(gamePlayMain);
                fromSplash = false;
                break;
            case MOVE_HISTORY_STATE:
                mainPane.setCenter(currentMovePane);
                break;
            case FLIGHT_PLAN_STATE:
                flightS.toFront();
                break;
            default:
        }
    }

}
