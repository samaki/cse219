package JTE.game;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Kwun Chan
 */
@XmlRootElement(name = "cities")
@XmlAccessorType(XmlAccessType.FIELD)
public class CityWrapper {

     @XmlElement(name = "card")
    private ArrayList<City> cities = new ArrayList<>();

    public ArrayList<City> getCities() {
        return cities;
    }

    public void setCities(ArrayList<City> city) {
        this.cities = city;
    }
}

