/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.game;

import javafx.scene.image.Image;

/**
 *
 * @author Kwun Chan
 */
public class CardData {

    private String color;
    private Image frontImg;
    private String cardName;

    public CardData(Image front, String col, String name) {
        color = col;
        frontImg = front;
        cardName = name;
    }

    public String getColor() {
        return this.color;
    }

    public Image getFront() {
        return this.frontImg;
    }

    public String getName() {
        return cardName;
    }
}
