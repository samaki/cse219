/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.game;

import JTE.ui.JourneyUI;
import java.util.ArrayList;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Kwun Chan
 */
public class JourneyGameStateManager {

    private JourneyUI ui;

    private JourneyGameData game;

    public JourneyGameStateManager(JourneyUI uii) {
        ui = uii;
        game = null;
    }

    public void makeNewGame(int selectedNum) {
        game = new JourneyGameData();
        ArrayList<BorderPane> panes = ui.getGameSetupPanes();

        for (int i = 0; i < selectedNum; i++) {
            BorderPane s = panes.get(i);
            VBox name = (VBox) s.getRight();
            TextField text = (TextField) name.getChildren().get(1);
            String s2 = text.getText();
            System.out.println(s2);
            VBox ra = (VBox) s.getCenter();
            RadioButton raBut = (RadioButton) ra.getChildren().get(0);
            RadioButton raBut2 = (RadioButton) ra.getChildren().get(1);
            boolean r1 = raBut.selectedProperty().get();
            boolean r2 = raBut2.selectedProperty().get();
            String type = " ";
            if (r1) {
                type = raBut.getText().trim();
            } else if (r2) {
                type = raBut2.getText().trim();
            }
            System.out.println(type);
            JourneyPlayer player = new JourneyPlayer((i + 1), s2, type);
            game.getPlayers().add(player);
        }
    }

    public void startNewGame() {
        ArrayList<City> cit = ui.getMap().getCities();
        ArrayList<CardData> green = new ArrayList<>();
        ArrayList<CardData> red = new ArrayList<>();
        ArrayList<CardData> yellow = new ArrayList<>();
       
        for (City ci : cit) {
            String iPath = ci.getCardPath();
            String imagePath = "file:images/" + iPath;
            Image cardImage = new Image(imagePath);
            CardData card = new CardData(cardImage, ci.getColor(), ci.getCityName());
            switch (card.getColor()) {
                case "Green":
                    green.add(card);
                    break;
                case "Red":
                    red.add(card);
                    break;
                case "Yellow":
                    yellow.add(card);
                    break;
            }
        }
        game.setGreen(green);
        game.setRed(red);
        game.setYellow(yellow);
    }

    public JourneyGameData getGame() {
        return game;
    }
}
