/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.game;

import java.util.ArrayList;

/**
 *
 * @author Kwun Chan
 */
public class JourneyPlayer {
    
    String playerName;
    int playerNumber;
    String playerType;
    ArrayList<CardData> playerCards;
    ArrayList<String> currentTurn;
    ArrayList<String> totalMoves;
    ArrayList<Integer> rolls;
    String currentCity;
    int movePoints;
    boolean stoppedForHarb;
    boolean rollAgain;
    boolean justFlew;
    boolean fly;
    
    public JourneyPlayer(int num, String name, String type)
    {
        playerNumber = num;
        playerName = name;
        playerType = type;
        playerCards = new ArrayList<>();
        currentTurn = new ArrayList<>();
        totalMoves = new ArrayList<>();
        rolls = new ArrayList<>();
        movePoints = 0;
        stoppedForHarb = false;
        rollAgain = false;
        justFlew = false;
        fly = false;
    }
    
    public String getPlayerName()
    {
        return playerName;
    }
    public ArrayList<CardData> getCards()
    {
        return playerCards;
    }
    
    public int getPlayerNum()
    {
        return playerNumber;
    }
    
    public void setCurrentCity(String name)
    {
        currentCity = name;
    }
    
    public String getCurrentCity()
    {
        return currentCity;
    }
    
    public int getMovePoints()
    {
        return movePoints;
    }
    
    public void setMovePoints(int moves)
    {
        movePoints = moves;
    }
    
    public boolean getHarb()
    {
        return stoppedForHarb;
    }
    public void setHarb(boolean x)
    {
        stoppedForHarb = x;
    }
    
    public boolean getRAgain()
    {
        return rollAgain;
    }
    
    public void setRollAgain(boolean r)
    {
        rollAgain = r;
    }
    
    public void setJustFlew(boolean t)
    {
        justFlew = t;
    }
    
    public boolean getJustFlew()
    {
        return justFlew;
    }
    
    public ArrayList<String> getThisTurnMoves()
    {
        return currentTurn;
    }
    
    public ArrayList<String> getTotalMoves()
    {
        return totalMoves;
    }
    
    public ArrayList<Integer> getRolls()
    {
        return rolls;
    }
    
    public boolean getFly()
    {
        return fly;
    }
    
    public void setFly(boolean f)
    {
        fly = f;
    }
}
