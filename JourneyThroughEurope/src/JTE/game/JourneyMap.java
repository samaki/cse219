/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.game;

import java.util.ArrayList;

/**
 *
 * @author Kwun Chan
 */
public class JourneyMap {

    private ArrayList<City> cities;

    public JourneyMap() {
        cities = new ArrayList<>();
    }

    public void setCities(ArrayList<City> cit) {
        cities = cit;
    }

    public ArrayList<City> getCities() {
        return this.cities;
    }

}
