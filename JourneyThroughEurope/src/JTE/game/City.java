/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.game;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Kwun Chan
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class City {

    @XmlElement(name = "x")
    private int xCoordi;

    @XmlElement(name = "y")
    private int yCoordi;

    @XmlElement(name = "name")
    private String cityName;

    @XmlElement(name = "color")
    private String color;

    @XmlElement(name = "section")
    private int quarter;

     @XmlElementWrapper(name = "land")
     @XmlElements(
     @XmlElement(name = "city")
     )
     public ArrayList<String> landNeighbors;

     @XmlElementWrapper(name = "sea")
     @XmlElements(
     @XmlElement(name = "city")
     )
     public ArrayList<String> seaNeighbors;

     //@XmlElement(name = "airport")
     public int flightSection;
     
    @XmlElement(name = "image")
    private String cardImageName;

    @XmlElement(name = "airport")
    private int airport;
    
    @XmlElement(name = "flightX")
    private int flightX;
    
    @XmlElement(name = "flightY")
    private int flightY;
    
    @XmlElement(name = "info")
    private String info;
    
    public City() {
    }

    public int getXCoord() {
        return this.xCoordi;
    }

    public int getYCoord() {
        return this.yCoordi;
    }

    public String getCityName() {
        return this.cityName;
    }

    public String getColor() {
        return this.color;
    }

    public int getQuarter() {
        return this.quarter;
    }

    public String getCardPath() {
        return this.cardImageName;
    }

    public void setXCoord(int xC) {
        xCoordi = xC;
    }

    public void setYCoord(int yC) {
        yCoordi = yC;
    }
    
    public void setCityName(String name) {
        cityName = name;
    }
    
    public void setColor(String col) {
        color = col;
    }
    
    public void setQuarter(int quart) {
        quarter = quart;
    }
    
    public void setCardImage(String cI) {
        cardImageName = cI;
    }
    public String toString() {
        String s = "" + xCoordi;
        return s;
    }

    public int getAirport()
    {
        return airport;
    }
    
    public int getFlightX()
    {
        return flightX;
    }
    
    public int getFlightY()
    {
        return flightY;
    }
    
    public String getInfo()
    {
    return info;
    }
}
