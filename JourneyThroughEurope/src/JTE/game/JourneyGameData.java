/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JTE.game;

import java.util.ArrayList;

/**
 *
 * @author Kwun Chan
 */
public class JourneyGameData {

    ArrayList<JourneyPlayer> players;
    ArrayList<CardData> greenCards;
    ArrayList<CardData> redCards;
    ArrayList<CardData> yellowCards;
    int playerTurn;
    boolean firstTurn;
    
    private final String[] colors = {"Red", "Green", "Yellow"};
    
    public JourneyGameData() {

        players = new ArrayList<>();
        greenCards = new ArrayList<>();
        redCards = new ArrayList<>();
        yellowCards = new ArrayList<>();
        playerTurn = 1;
        firstTurn = true;
    }

    public ArrayList<JourneyPlayer> getPlayers() {
        return this.players;
    }
    
    public ArrayList<CardData> getGreen()
    {
        return greenCards;
    }
    
    public ArrayList<CardData> getRed()
    {
        return redCards;
    }
    
    public ArrayList<CardData> getYellow()
    {
        return yellowCards;
    }
    
    public void setGreen(ArrayList<CardData> cards)
    {
        greenCards = cards;
    }
    
    public void setRed(ArrayList<CardData> cards)
    {
        redCards = cards;
    }
    
    public void setYellow(ArrayList<CardData> cards)
    {
        yellowCards = cards;
    }
    
    public String[] getColors()
    {
        return this.colors;
    }
    
    public int getPlayerTurn()
    {
        return playerTurn;
    }
    
    public void setPlayerTurn(int turn)
    {
        playerTurn = turn;
    }
    
    public boolean getFirst()
    {
        return firstTurn;
    }
    
    public void setFirst(boolean s)
    {
        firstTurn = s;
    }
    
}
