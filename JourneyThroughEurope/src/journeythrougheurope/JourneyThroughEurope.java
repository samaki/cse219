/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package journeythrougheurope;

import JTE.ui.JourneyUI;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Kwun Chan
 */
public class JourneyThroughEurope extends Application {
    
    static String PROPERTY_TYPES_LIST = "property_types.txt";
    static String UI_PROPERTIES_FILE_NAME = "properties.xml";
    static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    static String DATA_PATH = "./data/";

    @Override
    public void start(Stage primaryStage) {
        try {
           PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(JourneyPropertyType.UI_PROPERTIES_FILE_NAME,
                    UI_PROPERTIES_FILE_NAME);
            props.addProperty(JourneyPropertyType.PROPERTIES_SCHEMA_FILE_NAME,
                    PROPERTIES_SCHEMA_FILE_NAME);
            props.addProperty(JourneyPropertyType.DATA_PATH.toString(),
                    DATA_PATH);
            props.loadProperties(UI_PROPERTIES_FILE_NAME,
                    PROPERTIES_SCHEMA_FILE_NAME);

            // GET THE LOADED TITLE AND SET IT IN THE FRAME
            /*String title = props.getProperty(HangManPropertyType.SPLASH_SCREEN_TITLE_TEXT);
            primaryStage.setTitle(title);*/

            JourneyUI root = new JourneyUI();
            BorderPane mainPane = root.GetMainPane();
            root.SetStage(primaryStage);

            // SET THE WINDOW ICON
           /* String mainPaneIconFile = props.getProperty(HangManPropertyType.WINDOW_ICON);
            Image mainPaneIcon = root.loadImage(mainPaneIconFile);
            primaryStage.getIcons().add(mainPaneIcon);*/

            Scene scene = new Scene(mainPane, mainPane.getWidth(), mainPane.getHeight());
            //scene.getStylesheets().add("file:src/journeythrougheurope/button.css");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public enum JourneyPropertyType {
        /* SETUP FILE NAMES */

        UI_PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME,
        /* DIRECTORIES FOR FILE LOADING */
        DATA_PATH, IMG_PATH,
        /* WINDOW DIMENSIONS */
        WINDOW_WIDTH, WINDOW_HEIGHT,
        /* LANGUAGE OPTIONS PROPERTIES */
        LANGUAGE_OPTIONS, LANGUAGE_DATA_FILE_NAMES, LANGUAGE_IMAGE_NAMES,
        /* IMAGE FILE NAMES */
        PLAYER1_FLAG_NAME, PLAYER2_FLAG_NAME, PLAYER3_FLAG_NAME, PLAYER4_FLAG_NAME, PLAYER5_FLAG_NAME, PLAYER6_FLAG_NAME, SPLASH_SCREEN_IMAGE_NAME, MAP_FILE_NAME, SECTOR1_FILE, SECTOR2_FILE, SECTOR3_FILE, SECTOR4_FILE, DIE1_FILE, DIE2_FILE, DIE3_FILE, DIE4_FILE, DIE5_FILE, DIE6_FILE,
        PLAYER1_PIECE, PLAYER2_PIECE, PLAYER3_PIECE, PLAYER4_PIECE, PLAYER5_PIECE, PLAYER6_PIECE, FLIGHT_FILE,
        /* DATA FILE STUFF */
        MOVE_FILE_NAME, CITIES_FILE, ABOUT_FILE_NAME,NEIGHBOR_FILE,
     
    }
}
