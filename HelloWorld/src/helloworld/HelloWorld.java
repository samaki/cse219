

package helloworld;

/**
 *
 * @author Kwun Chan
 */
public class HelloWorld {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Prints out Hello World!
        System.out.println("Hello World!");
    }
    
}
